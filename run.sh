#! /bin/bash

# Exit at first error
set -e

# Prerequisites:
# - sudo apt install tmux
# - sudo apt install python3 python3-pip

SESSION_NAME="server2"
START_CMD="PYTHONPATH=. pipenv run python cou_server/server.py"

echo "Installing pipenv..."
python3 -m pip install pipenv
echo "...done installing pipenv."

echo "Installing dependencies..."
pipenv install
echo "...done installing dependencies."

echo "Killing old server process (if any)..."
tmux kill-session -t $SESSION_NAME || true
echo "...done killing old server process (if any)."

echo "Starting server..."
pipenv run tmuxp load -d tmuxp-session.yaml
echo "...done starting server."
