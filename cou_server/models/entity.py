from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship, backref

from cou_server.common import get_logger
from cou_server.common.database import Base
from cou_server.metabolics.metabolics_change import MetabolicsChange

logger = get_logger(__name__)


class Entity(Base, MetabolicsChange):
    __tablename__ = "street_entities"

    id = Column(String, primary_key=True)
    type = Column(String, nullable=False)
    tsid = Column(String)
    x = Column(Integer, nullable=False, default=0)
    y = Column(Integer, nullable=False, default=0)
    z = Column(Integer, nullable=False, default=0)
    h_flip = Column(Boolean, default=False)
    rotation = Column(Integer, default=0)
    metadata_json = Column(String, nullable=False, default="{}")

    def __repr__(self):
        return f'Entity(id="{self.id}", type="{self.type}")'

    @staticmethod
    def set_entity(entity, load_now=True, load_db=True):
        from cou_server.endpoints import mapdata, street_update

        def _set_in_db(entity):
            with db_session_context() as db_session:
                try:
                    db_session.merge(entity)
                    return True
                except Exception:
                    logger.exception(f"Could not edit entity {entity}")
                    return False

        def _set_in_memory(entity):
            street = mapdata.get_street_by_tsid(entity.tsid)
            if street:
                # if the street isn't currently loaded, then just return
                if street_update.streets.get(street["label"]) is None:
                    logger.warning(
                        f"Tried to set entity <id={entity.id}> on unloaded street <tsid={entity.tsid}>"
                    )
                    return True

                return street_update.streets[street["label"]].put_entities_in_memory(
                    [entity]
                )

            return False

        if load_db and not _set_in_db(entity):
            return False
        if load_now and not _set_in_memory(entity):
            return False

        return True

    @staticmethod
    def delete_entity(entity_id):
        from cou_server.endpoints import street_update

        def _delete_from_db():
            with db_session_context() as db_session:
                try:
                    db_session.query(StreetEntity).get(entity_id).delete()
                    return True
                except Exception:
                    logger.exception(
                        f"COuld not delete entity {entity_id} from database"
                    )
                    return False

        def _delete_from_memory():
            try:
                street_update.queue_npc_remove(entity_id)
                return True
            except Exception:
                logger.exception(f"Could not delete entity {entity_id} from memory")
                return False

        if not _delete_from_db():
            return False
        else:
            return _delete_from_memory()


class EntitySchema(ModelSchema):
    class Meta:
        model = Entity
