import json

from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy import Column, DateTime, Integer, String

from cou_server.common.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False)
    email = Column(String, nullable=False)
    bio = Column(String)
    custom_avatar = Column(String)
    username_color = Column(String, default="#      ")
    elevation = Column(String, default="_")
    last_login = Column(DateTime)
    _friends = Column("friends", String, default="[]")

    def __repr__(self):
        return f'User(id="{self.id}", username="{self.username}", email="{self.email}")'

    def get_friends(self, db_session):
        return (
            db_session.query(User).filter(User.id.in_(json.loads(self._friends))).all()
        )

    def set_friends(self, db_session, new_friends):
        self._friends = json.dumps([f.id for f in new_friends])


class UserSchema(ModelSchema):
    class Meta:
        model = User
