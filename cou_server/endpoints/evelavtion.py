from flask import Blueprint, jsonify, request
from flask_cors import CORS

from cou_server import api_keys
from cou_server.common.database import db_session_context
from cou_server.models.user import User

elevation_endpoint = Blueprint("elevation_endpoint", __name__, url_prefix="/elevation")
CORS(elevation_endpoint, automatic_options=True)

DEFAULT = "_"
_cache = {}


@elevation_endpoint.route("/get/<string:username>")
def elevation_get(username):
    if username in _cache:
        return _cache[username]

    with db_session_context() as db_session:
        user = db_session.query(User).filter(User.username == username).first()
        elevation_str = DEFAULT
        if user:
            elevation_str = user.elevation
        _cache[username] = elevation_str
        return elevation_str


@elevation_endpoint.route("/list/<string:status>")
def list_status(status):
    response = []
    with db_session_context() as db_session:
        users = db_session.query(User).filter(User.elevation == status).all()
        for user in users:
            last_login = user.last_login if user.last_login else "never"
            response.append({"username": user.username, "last_login": last_login})

    return jsonify(response)


@elevation_endpoint.route("/set")
def set_elevation():
    token = request.args.get("token")
    channel = request.args.get("channel")
    text = request.args.get("text")

    if token != api_keys.SLACK_PROMOTE_TOKEN:
        return "Invalid token"

    if channel != "G0277NLQS":
        return "Run this from the administration group"

    try:
        parts = text.split(" ")
        elevation = parts[0].strip()
        username = parts[1:].join(" ")
    except:
        return "Invalid paramters"

    if elevation == "none":
        elevation = DEFAULT

    with db_session_context() as db_session:
        user = db_session.query(User).filter(User.username == username).first()
        user.elevation = elevation
        _cache[username] = elevation
        if elevation != DEFAULT:
            suffix = f"a {elevation}"
        else:
            suffix = "demoted"
        return f"{username} is now {suffix}."
