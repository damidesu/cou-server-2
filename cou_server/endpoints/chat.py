import json

from flask import Blueprint

from cou_server import MIN_CLIENT_VER
from cou_server.common import get_logger
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import toast


logger = get_logger(__name__)
chat_blueprint = Blueprint("chat_blueprint", __name__)

users = {}


class ChatEvent:
    def __init__(
        self, username=None, message=None, channel=None, street_name=None, **kwargs
    ):
        self.username = username
        self.message = message
        self.channel = channel
        self.street_name = street_name


class Identifier:
    def __init__(self, username, current_street, tsid, web_socket):
        self.channel_list = []
        self.username = username
        self.current_street = current_street
        self.tsid = tsid
        self.current_x = 1.0
        self.current_y = 0.0
        self.web_socket = web_socket

    def __repr__(self):
        return f"<Identifier for {self.username} on {self.tsid} at ({self.current_x}, {self.current_y})"


@chat_blueprint.route("/chat")
def chat_ws(socket):
    while not socket.closed:
        try:
            data = json.loads(socket.receive())
        except Exception as e:
            break

        if data.get("exempt", None) is None:
            channel = data.get("channel", "")
            status_message = data.get("statusMessage", None)
            username = data.get("username", None)
            message = data.get("message", None)
            muted = False  # TODO
            if (
                channel == "Global Chat"
                and not status_message
                and username
                and message
                and not muted
            ):
                slackSend(username, message)
            processMessage(socket, data)
    cleanup_lists(socket)


def processMessage(socket, message):
    data = message
    client_version = data.get("clientVersion", None)
    exempt = data.get("exempt", False)
    channel = data.get("channel", "")
    username = data.get("username", "")
    muted = False  # TODO
    status_message = data.get("statusMessage", "")
    street = data.get("street", "")
    tsid = data.get("tsid", "")
    new_street_label = data.get("newStreetLabel", "")
    old_street_label = data.get("oldStreetLabel", "")
    old_street_tsid = data.get("oldStreetTsid", "")
    new_street_tsid = data.get("newStreetTsid", "")
    new_street = data.get("newStreet", "")

    if client_version is not None and client_version < MIN_CLIENT_VER:
        socket.send(
            json.dumps({"error": "Your client is outdated. Please reload the page."})
        )
        return

    if not exempt and channel == "Global Chat" and muted:
        socket.send(
            json.dumps(
                {
                    "muted": "true",
                    "toastText": (
                        "You may not use Global Chat because you are a nuisance to Ur."
                        " Please click here to email us if you believe this is an error."
                    ),
                    "toastClick": "__EMAIL_COU__",
                }
            )
        )
        return

    if status_message == "join":
        data["statusMessage"] = "true"
        data["message"] = " joined."
        users[username] = Identifier(username, street, tsid, socket)
        users[username].channel_list.extend([street, "Global Chat"])
    elif status_message == "changeStreet":
        already_sent = []
        for user_name, user_id in users.items():
            if user_name == username:
                user_id.current_street = new_street_label
                if old_street_label in user_id.channel_list:
                    user_id.channel_list.remove(old_street_label)
                user_id.channel_list.append(new_street_label)
            if (
                user_id.username not in already_sent
                and user_id.username != username
                and user_id.current_street == old_street_tsid
            ):
                left_for_message = {
                    "statusMessage": "leftStreet",
                    "username": username,
                    "streetName": new_street_label,
                    "tsid": new_street_tsid,
                    "message": " has left for ",
                    "channel": "Local Chat",
                }
                if users[user_id.username]:
                    users[user_id.username].web_socket.send(
                        json.dumps(left_for_message)
                    )
                already_sent.append(user_id.username)
            if user_id.current_street == new_street and user_id.username != username:
                toast(f"{username} is here!", user_id.web_socket)
        return
    elif status_message == "list":
        user_list = []
        for user_name, user_id in users.items():
            if channel == "Local Chat" and user_id.current_street == street:
                user_list.append(user_id.username)
            elif channel != "Local Chat":
                user_list.append(user_id.username)
        data["users"] = user_list
        data["message"] = "Users in this channel: "
        users[username].web_socket.send(json.dumps(data))
        return

    MessageBus().publish("chat_event", ChatEvent(**data))
    send_all(json.dumps(data))


def slackSend(username, message):
    # TODO
    pass


def cleanup_lists(socket, reason="No reason given"):
    leaving_user = None
    for user_name, user_id in users.items():
        if user_id.web_socket == socket:
            user_id.web_socket = None
            leaving_user = user_name
    users.pop(leaving_user)

    # send a message to all other clients that this user has disconnected
    data = {"message": " left.", "channel": "Local Chat", "username": leaving_user}
    send_all(json.dumps(data))


def send_all(message):
    for user_name, user_id in users.items():
        user_id.web_socket.send(message)
