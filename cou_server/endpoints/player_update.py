from datetime import datetime
import json
import math

from flask import Blueprint

from cou_server import MIN_CLIENT_VER
from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.message_bus import MessageBus
from cou_server.endpoints import letters
from cou_server.models.user import User
from cou_server.quests.messages import RequirementProgress, PlayerPosition


logger = get_logger(__name__)
player_blueprint = Blueprint("player_blueprint", __name__)

users = {}
message_post_counter = {}


class Identifier:
    def __init__(self, username, current_street, tsid, socket):
        self.username = username
        self.current_street = current_street
        self.tsid = tsid
        self.socket = socket
        self.channel_list = []
        self.current_x = 1.0
        self.current_y = 0.0

    def __str__(self):
        return f"<Identifier for {self.username} on {self.tsid} at ({self.current_x, self.current_y})>"


@player_blueprint.route("/playerUpdate")
def chat_ws(socket):
    while not socket.closed:
        try:
            data = json.loads(socket.receive())
        except Exception as e:
            break

        processMessage(socket, data)


def processMessage(socket, message):
    try:
        data = message
        if data.get("clientVersion") is not None:
            if data["clientVersion"] < MIN_CLIENT_VER:
                socket.send(json.dumps({"error": "version to low"}))
            return

        username = data.get("username")
        email = data.pop("email", None)
        if username in users:
            # we've had an update for this user before
            previous_street = users[username].current_street
            if previous_street != data.get("street"):
                # the user must have switched streets

                if data.get("street") == "Louise Pasture":
                    # offer the race to the forest
                    with db_session_context() as db_session:
                        quest_log = (
                            db_session.query(User)
                            .filter(User.username == username)
                            .first()
                            .quest_log
                        )
                        quest_log.offer_quest("Q4")

                MessageBus().publish(
                    "player_location_change",
                    RequirementProgress(f"location_{data['street']}", email),
                )

                data["changeStreet"] = data["street"]
                data["previousStreet"] = previous_street
                users[username].current_street = data["street"]

            data["letter"] = letters.new_player_letter(username)

            try:
                prev_x = users[username].current_x
                prev_y = users[username].current_y
                current_x = float(data["xy"].split(",")[0])
                current_y = float(data["xy"].split(",")[1])
                x_diff = abs(current_x - prev_x)
                y_diff = abs(current_y - prev_y)

                new_steps = abs(math.ceil((x_diff + y_diff) / 22))
                # TODO StatManager.add(email, Stat.steps_taken, increment=new_steps, buffer=True)

                if y_diff > 17:
                    pass
                    # TODO: detect this better
                    # TODO StatManager.add(email, Stat.jumps, buffer=True)

                users[username].current_x = current_x
                users[username].current_y = current_y
                # limit the number of position messages that get broadcast
                # 1 message every 5gh at 30 messages per second would be 6 per second
                if (
                    message_post_counter.get(email) is None
                    or message_post_counter[email] > 5
                ):
                    MessageBus().publish(
                        "player_position",
                        PlayerPosition(data["street"], email, current_x, current_y),
                    )
                    message_post_counter[email] = 0
                else:
                    message_post_counter[email] += 1
            except Exception:
                logger.exception("Error processing player update")
        else:
            # this user must have just connected
            users[username] = Identifier(username, data["street"], data["tsid"], socket)
            data["letter"] = letters.new_player_letter(username)

            # update last login date
            with db_session_context() as db_session:
                db_session.query(User).filter(
                    User.username == username
                ).first().last_login = datetime.now()

            try:
                current_x = float(data["xy"].split(",")[0])
                current_y = float(data["xy"].split(",")[1])
                users[username].current_x = current_x
                users[username].current_y = current_y
            except Exception:
                logger.exception("Error processing player update")

        send_all(data)

    except Exception:
        logger.exception("Error processing player message")


def send_all(data):
    for username, identifier in users.items():
        if (
            data["street"] == identifier.current_street
            or data.get("changeStreet") is not None
        ):
            try:
                identifier.socket.send(json.dumps(data))
            except Exception:
                pass
