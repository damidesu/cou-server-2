import json
import math
import time

from flask import Blueprint, jsonify, request
from flask_cors import CORS
import gevent

from cou_server import MIN_CLIENT_VER
from cou_server import entities
from cou_server.common import get_logger, timed
from cou_server.common.database import db_session_context
from cou_server.common.util import prompt_callbacks, wait_for
from cou_server.entities.action import Action, ActionSchema
from cou_server.entities.npcs.npc import NPC
from cou_server.endpoints.metabolics import update_death, add_quoin, deny_quoin
from cou_server.endpoints.player_update import users
from cou_server.models.user import User
from cou_server.streets.street import Street


logger = get_logger(__name__)
street_blueprint = Blueprint("street_blueprint", __name__)
street_ws_blueprint = Blueprint("street_ws_blueprint", __name__)
CORS(street_blueprint, automatic_options=True)

streets = {}
user_sockets = {}


def simulate_streets():
    """Run the simulation loop forever"""

    logger.debug("Simulating all loaded streets forever")

    @timed
    def sim_loop():
        to_remove = []
        for street_name, street in streets.items():
            if not street.simulate():
                to_remove.append(street_name)

        for street_name in to_remove:
            streets.pop(street_name, None)

    while True:
        sim_loop()
        time.sleep(1)


gevent.spawn(simulate_streets)


@street_ws_blueprint.route("/streetUpdate")
def chat_ws(socket):
    while not socket.closed:
        try:
            data = json.loads(socket.receive())
        except Exception as e:
            break

        processMessage(socket, data)


def load_street(street_name: str, tsid: str) -> None:
    street_name = street_name.strip()
    try:
        with db_session_context() as db_session:
            street = db_session.query(Street).filter(Street.tsid == tsid).first()
            street.street_name = street_name
            streets[street_name] = street
            street.load_items()
            street.load_json()
            street.loaded = True
            logger.debug(
                f"Loaded street <street_name={street_name}> ({tsid}) into memory"
            )
    except Exception:
        logger.exception(f"Could not load street {tsid}")
        raise


def cleanup_list(socket):
    # find and remove ws from whichever street has it
    user_to_remove = None
    for streetname, street in streets.items():
        for username, sock in street.occupants.items():
            if sock == socket:
                user_to_remove = username
        if user_to_remove:
            street.occupants.pop(user_to_remove, None)
    for email, sock in users.items():
        if sock == socket:
            user_to_remove = username
    users.pop(user_to_remove, None)

    # stop updating their buffs
    # BuffManager.stop_updating_user(user_to_remove)


def processMessage(socket, message):
    # we should receive 3 kinds of messages:
    # layer enters street, player exits street, player interacts with object
    # everything else will be outgoing
    try:
        data = message
        # chat bubbles: button clicked, call callback
        if data.get("bubbleButton") is not None:
            try:
                button_id = int(data["bubbleButton"])
                NPC.pending_bubble_callbacks[button_id]()
            except Exception:
                logger.exception(
                    f"Could not call callback for chat bubble button <id={data['bubbleButton']}>"
                )
            return

        # string prompt window: send reference and response to callback
        if data.get("promptRef") is not None:
            try:
                ref = data["promptRef"]
                response = data.get("promptResponse", "")
                prompt_callbacks[ref](ref, response)
            except Exception:
                logger.exception(
                    f"Could not call callback for string prompt <ref={data['promptRef']}"
                )
            return

        street_name = data.get("streetName", "").strip()
        username = data.get("username", "").strip()
        email = data.get("email", "").strip()
        if not email:
            with db_session_context() as db_session:
                email = (
                    db_session.query(User)
                    .filter(User.username == username)
                    .first()
                    .email
                )

        # if the street doesn't yet exist, create it (maybe it got stored back to the datastore)
        if not street_name in streets:
            load_street(street_name, data["tsid"])

        # a player has joined or left the street
        if data.get("message") == "joined":
            # set this player as being on this street
            if username in users:
                users[username].tsid = data["tsid"]

            if "clientVersion" in data and data["clientVersion"] < MIN_CLIENT_VER:
                socket.send(json.dumps({"error": "version too low"}))
                return
            else:
                user_sockets[email] = socket
                try:
                    streets[street_name].occupants[username] = socket
                except Exception:
                    logger.warning(
                        f"Adding {username} to {street_name}. Waiting to retry..."
                    )
                    try:
                        wait_for(lambda: streets[street_name].loaded, timeout=10)
                        streets[street_name].occupants[username] = socket
                    except Exception:
                        logger.exception(
                            f"Adding {username} to {street_name}. Giving up"
                        )

                with db_session_context() as db_session:
                    m = (
                        db_session.query(User)
                        .filter(User.username == username)
                        .first()
                        .metabolics
                    )
                    m.location_history.append(data["tsid"])

                if data["firstConnect"]:
                    # Inventory.fire_inventory_at_user(socket, email)
                    update_death(users.get(username), True)
                    # BuffManager.start_updating_user(email)

                # These will automatically disregard false calls, so call both every time
                # WintryPlaceHandler.enter(street_name, email, socket)
                # SavannaHandler.enter(street_name, email, socket)
                return
        elif data.get("message") == "left":
            cleanup_list(socket)

            # These will automatically disregard false calls, so call both every time
            # WintryPlaceHandler.enter(street_name, email, socket)
            # SavannaHandler.enter(street_name, email, socket)
            return

        if data.get("remove") is not None:
            if data["type"] == "quoin":
                touched = streets[street_name].quoins[data["remove"]]
                player = users[username]

                if player is None:
                    logger.warning(f"Could not find player {username} to collect quoin")
                elif touched is not None and not touched.collected:
                    xDiff = abs(touched.x - player.current_x)
                    yDiff = abs(touched.y - player.current_y)
                    diff = math.sqrt(pow(xDiff, 2) + pow(yDiff, 2))

                    if diff < 500:
                        add_quoin(touched, username)
                    else:
                        deny_quoin(touched, username)
                        logger.debug(f"Denied quoin to {username}: too far away {diff}")
                elif touched is None:
                    logger.warning(
                        f"Could not collect quoin {data['remove']} for player {username}: quoin not found"
                    )

            return

        if data.get("callMethod") is not None:
            if data["id"] == "global_action_monster":
                _call_global_method(data, socket, email)
                return
            else:
                entity_type = data["type"].replace("entity", "").strip()
                entity_type = entity_type.replace("groundItemGlow", "").strip()
                entity_map = streets[street_name].entity_maps[entity_type]
                method_name = normalize_method_name(data["callMethod"])
                arguments = {"socket": socket, "email": email}

                if entity_map is not None and entity_map[data["id"]] is not None:
                    try:
                        entity = entity_map[data["id"]]
                        if entity_type == "groundItem":
                            for action in entity.actions:
                                if (
                                    normalize_method_name(action.action_name)
                                    == method_name
                                    and action.ground_action
                                ):
                                    data["arguments"]["map"] = {
                                        "id": data["id"],
                                        "streetName": data["streetName"],
                                    }

                        if data["arguments"] is not None:
                            arguments.update(data["arguments"])
                        getattr(entity, method_name)(**arguments)
                    except Exception:
                        logger.exception(
                            f"Could not invoke entity method {method_name}"
                        )
                else:
                    # check if it's an item and not an entity
                    try:
                        entity = getattr(items, entity_type)
                        arguments["username"] = data["username"]
                        arguments["street_name"] = data["street_name"]
                        arguments["map"] = data["arguments"]
                        getattr(entity, method_name)(**arguments)
                    except Exception:
                        logger.exception(f"Could not invoke item method {method_name}")
                return
    except Exception:
        logger.exception("Error processing street update")


def _call_global_method(data, socket, email):
    pass


def normalize_method_name(name: str) -> str:
    new_name = ""
    parts = name.split(" ")
    for index, part in enumerate(parts):
        if index > 0:
            parts[index] = parts[index][0:1].upper() + parts[index][1:]
        new_name += parts[index]

    new_name = new_name[0:1].lower() + new_name[1:]
    return new_name


def queue_npc_remove(entity_id):
    pass


@street_blueprint.route("/getActions")
def get_actions():
    email = request.args.get("email")
    id = request.args.get("id")
    label = request.args.get("label")

    if email is None or id is None or label is None:
        logger.debug(
            f"<email={email}> tried to get actions for <id={id}> on <label={label}>"
        )
        return jsonify([])

    if not streets.get(label):
        logger.debug(f"<label={label}> is not a currently loaded street")
        return jsonify([])

    entity = streets[label].npcs.get(id)
    if not entity:
        entity = streets[label].plants.get(id)
    if not entity:
        entity = streets[label].doors.get(id)
    if not entity:
        entity = streets[label].ground_items.get(id)
    if not entity:
        # are they a player?
        if users.get(id):
            follow_action = Action(action="follow")
            follow_action.description = (
                "We already know it's buggy, but we thought you'd have fun with it."
            )
            profile_action = Action(action="profile")
            return jsonify([follow_action, profile_action])
    if not entity:
        logger.debug(f"<id={id}> is not a valid entity on <label={label}>")
        return jsonify([])

    return ActionSchema().dumps(entity.customize_actions(email), many=True)
