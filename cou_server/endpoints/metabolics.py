from datetime import datetime
import math
import json
import random
import time

from flask import Blueprint, Response, jsonify, request
from flask_cors import CORS
import gevent
from sqlalchemy import or_
from werkzeug.exceptions import NotFound, BadRequest

from cou_server.common import constants, get_logger
from cou_server.common.database import db_session_context
from cou_server.common.util import clamp
from cou_server.endpoints import chat, mapdata, player_update
from cou_server.entities.plants.respawning_items.hellgrapes import HellGrapes
from cou_server.models.metabolics import Metabolics, MetabolicsSchema, img_levels
from cou_server.models.user import User


metabolics_endpoint = Blueprint("metabolics_endpoint", __name__)
metabolics_ws_blueprint = Blueprint(
    "metabolics_ws_blueprint", __name__, url_prefix="/metabolics"
)
CORS(metabolics_endpoint, automatic_options=True)


logger = get_logger(__name__)
user_sockets = {}
GLOBAL_QUOIN_MULTIPLIER = 1


@metabolics_ws_blueprint.route("")
def metabolics_ws(socket):
    while not socket.closed:
        try:
            data = json.loads(socket.receive())
        except Exception as e:
            break

        username = data["username"]
        if username not in user_sockets.items():
            user_sockets[username] = socket

    # cleanup closed socket
    leaving_user = None
    for username, user_socket in user_sockets.items():
        if socket == user_socket:
            user_socket = None
            leaving_user = username

    user_sockets.pop(leaving_user, None)


last_simulation = datetime.now()
simulate_energy = False
simulate_mood = False


def simulate_timer():
    logger.debug("Simulating metabolics forever")
    while True:
        simulate()
        time.sleep(5)


def energy_timer():
    global simulate_energy
    while True:
        simulate_energy = True
        time.sleep(90)


def mood_timer():
    global mood_timer
    while True:
        simulate_mood = True
        time.sleep(60)


gevent.spawn(simulate_timer)
gevent.spawn(energy_timer)
gevent.spawn(mood_timer)


def simulate():
    global last_simulation
    for username, socket in user_sockets.items():
        try:
            with db_session_context() as db_session:
                user = db_session.query(User).filter(User.username == username).first()
                m = user.metabolics
                email = user.email

                # effects from smashed/hungover buffs
                SMASHED_RATE = 3  # - energy, + mood
                SMASHED_TIME = 30  # seconds

                HUNGOVER_RATE = 17  # - mood
                HUNGOVER_TIME = 5  # seconds

                is_smashed = (
                    False
                )  # TODO: BuffManager.player_has_buff('smashed', email)
                is_hungover = (
                    False
                )  # TODO: BuffManager.player_has_buff('hungover', email)
                if (
                    abs(datetime.now() - last_simulation).seconds >= SMASHED_TIME
                    and is_smashed
                ):
                    # smashed converts energy into mood
                    # energy will never go below HOOCH_RATE with this buff
                    m.energy = clamp(
                        m.energy - SMASHED_RATE, SMASHED_RATE, m.max_energy
                    )
                    m.mood += SMASHED_RATE
                elif (
                    abs(datetime.now() - last_simulation).seconds >= HUNGOVER_TIME
                    and is_hungover
                ):
                    # hungover is a mood killer
                    m.mood -= HUNGOVER_RATE

                if simulate_mood:
                    _calc_and_set_mood(m)

                if m.mood < m.max_mood // 10:
                    user.quest_log.offer_quest("Q10")

                if simulate_energy:
                    _calc_and_set_energy(m)

                user_identifier = player_update.users.get(username)
                if user_identifier:
                    update_death(user_identifier, m)

                # store current street and position
                if user_identifier:
                    m.current_street = user_identifier.tsid
                    m.current_street_x = user_identifier.current_x
                    m.current_street_y = user_identifier.current_y
                    # send the metabolics back to the user
                    socket.send(MetabolicsSchema().dumps(m))
        except Exception:
            logger.exception("Metabolics simulation failed")

        last_simulation = datetime.now()


def _calc_and_set_energy(m):
    global simulate_mood
    max_mood = m.max_mood
    mood_ratio = m.mood / max_mood

    # determine how much mood they should lose based on current percentage of max
    # https://web.archive.org/web/20130106191352/http://www.glitch-strategy.com/wiki/Mood
    if mood_ratio < 0.5:
        m.mood -= math.ceil(max_mood * 0.005)
    elif 0.81 < mood_ratio <= 0.5:
        m.mood -= math.ceil(max_mood * 0.01)
    else:
        m.mood -= math.ceil(max_mood * 0.015)

    # keep between 0 and max (both inclusive)
    m.mood = clamp(m.mood, 0, m.max_mood)
    simulate_mood = False


def _calc_and_set_energy(m):
    global simulate_energy
    # players lose .8% of their max energy every 90 seconds
    # https://web.archive.org/web/20120805062536/http://www.glitch-strategy.com/wiki/Energy
    m.energy -= math.ceil(m.max_energy * 0.008)

    # keep between 0 and max (both inclusive)
    m.energy = clamp(m.energy, 0, m.max_energy)
    simulate_energy = False


@metabolics_endpoint.route("/getMetabolics")
def get_metabolics(username=None, email=None, user_id=None, case_sensitive=False):
    if request:
        username = request.args.get("username")
        email = request.args.get("email")
        user_id = request.args.get("user_id")
        case_sensitive = request.args.get("case_sensitive", False)

    with db_session_context() as db_session:
        query = db_session.query(Metabolics).join(Metabolics.user)
        if email:
            query.filter(User.email == email)
        elif username:
            if not case_sensitive:
                query.filter(User.username.ilike(username))
            else:
                query.filter(User.username.like(username))
        elif user_id:
            query.filter(User.id == user_id)

        metabolic = query.first()
        if not metabolic:
            metabolic = Metabolics()
            db_session.add(metabolic)
            user = (
                db_session.query(User)
                .filter(
                    or_(
                        User.username == username,
                        User.email == email,
                        User.id == user_id,
                    )
                )
                .first()
            )
            if not user:
                raise BadRequest(
                    f"Could not find user with username={username}, email={email}, user_id={user_id}."
                )
            metabolic.user = user
        return jsonify(MetabolicsSchema().dump(metabolic))


@metabolics_endpoint.route("/getLevel")
def get_level():
    img = request.args.get("img", type=int)

    if img >= img_levels[60]:
        result = 60
    else:
        for level in img_levels.keys():
            level_img = img_levels[level]
            if img < level_img:
                result = level - 1
                break

    return str(result)


@metabolics_endpoint.route("/getImgForLevel")
def get_img_for_level():
    lvl = request.args.get("level", type=int)

    if lvl > 0 and lvl <= 60:
        return str(img_levels[lvl])
    else:
        return str(-1)


def update_death(user_identifier, init=False):
    HELL_ONE = "GA5PPFP86NF2FOS"
    CEBARKUL = "GIF12PMQ5121D68"
    NARAKA = 40
    if not user_identifier:
        return

    with db_session_context() as db_session:
        m = (
            db_session.query(User)
            .filter(User.username == user_identifier.username)
            .first()
            .metabolics
        )
        if m.energy == 0 and (m.undead_street is None or init):
            m.dead = True

            # go to hell one
            user_identifier.socket.send(
                json.dumps({"gotoStreet": "true", "tsid": HELL_ONE})
            )
        elif m.energy >= HellGrapes.ENERGY_REQ:
            # enough energy to be alive

            street = mapdata.get_street_by_tsid(m.current_street)
            if street is None or street.get("hub_id", NARAKA) == NARAKA:
                # in Naraka, return to world
                user_identifier.socket.send(
                    json.dumps(
                        {
                            "gotoStreet": True,
                            "tsid": m.undead_street if m.undead_street else CEBARKUL,
                        }
                    )
                )

                m.dead = False


def add_quoin(quoin, username):
    with db_session_context() as db_session:
        user = db_session.query(User).filter(User.username == username).first()
        m = user.metabolics

        if m.quoins_collected >= constants.QUOIN_LIMIT:
            # Daily quoin limit
            deny_quoin(quoin, username)
            return

        amt = 0
        if quoin.type == "mystery":
            # choose a number 0.01 i to 0.09 i
            amt = random.randint(1, 9) / 100
            # add it to the player's quoin multiplier
            m.quoin_multiplier += amt

            # limit QM
            if m.quoin_multiplier > constants.QUOIN_MULTIPLIER_LIMIT:
                m.quoin_multiplier = constants.QUOIN_MULTIPLIER_LIMIT
                # add double quoins buff instead
                # TODO BuffManager.add_to_user('double_quoins', user.email, user_sockets[user.email])
        else:
            # choose a number 1-5
            amt = random.randint(1, 5)
            # multiply it by the player's quoin multiplier
            amt = round(amt * m.quoin_multiplier)
            amd = round(amt * GLOBAL_QUOIN_MULTIPLIER)
            # double if buffed
            # TODO: if BuffManager.player_has_buff('double_quoins', user.email):
            # amt *= 2

        if quoin.type == "quarazy":
            amt *= 7

        if quoin.type == "currant":
            m.currants += amt

        if quoin.type in ["img", "quarazy"]:
            m.img += amt
            m.lifetime_img += amt

        if quoin.type == "mood":
            if m.mood + amt > m.max_mood:
                amt = m.max_mood - m.mood
            m.mood += amt

        if quoin.type == "energy":
            if m.energy + amt > m.max_energy:
                amt = m.max_energy - m.energy
            m.energy += amt

        if quoin.type == "favor":
            for giant in [
                "alph",
                "cosma",
                "friendly",
                "grendaline",
                "humbaba",
                "lem",
                "mab",
                "pot",
                "spriggan",
                "tii",
                "zille",
            ]:
                giant_favor = getattr(m, f"{giant}favor")
                giant_favor_max = getattr(m, f"{giant}favor_max")
                giant_favor += amt
                giant_favor = clamp(giant_favor, 0, giant_favor_max - 1)

        if quoin.type == "time":
            pass
            # if BuffManager.player_has_buff('nostalgia', user.email):
            #     buff = BuffManager.buffs['nostalgia'].get_for_player(user.email)
            #     buff.extend(datetime.timedelta(seconds=amt))

        if amt > 0:
            m.quoins_collected += 1

        try:
            map = {
                "collectQuoin": "true",
                "id": quoin.id,
                "amt": amt,
                "quoinType": quoin.type,
            }
            quoin.set_collected(username)
            user_sockets[username].send(json.dumps(map))  # send quoin
            user_sockets[username].send(MetabolicsSchema().dumps(m))  # send metabolics
        except Exception:
            logger.exception(
                f"Could not set metabolics {m} for player {username} adding quoin"
            )


def deny_quoin(quoin, username):
    map = {"collectQuoin": "true", "success": "false", "id": quoin.id}
    try:
        user_sockets[username].send(json.dumps(map))
        return True
    except Exception:
        logger.exception(f"Could not pass map {map} to player {username} denying quoin")
        return False
