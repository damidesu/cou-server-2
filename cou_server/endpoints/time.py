import datetime

from flask import Blueprint, Response, request, jsonify
from flask_cors import CORS

time_blueprint = Blueprint("time_blueprint", __name__)
CORS(time_blueprint, automatic_options=True)

TZ_UTC = datetime.timezone(datetime.timedelta(hours=0))

EPOCH = datetime.datetime.utcfromtimestamp(0)

MONTH_NAMES = [
    "Primuary",
    "Spork",
    "Bruise",
    "Candy",
    "Fever",
    "Junuary",
    "Septa",
    "Remember",
    "Doom",
    "Widdershins",
    "Eleventy",
    "Recurse",
]

DAY_NAMES = [
    "Hairday",
    "Moonday",
    "Twoday",
    "Weddingday",
    "Theday",
    "Fryday",
    "Standday",
    "Fabday",
]

DAYS_PER_MONTH = [29, 3, 53, 17, 73, 19, 13, 37, 5, 47, 11, 1]

HOLIDAYS_BY_MONTH = {
    "Primuary": {5: ["AlphaCon"]},
    "Spork": {2: ["Lemadan"]},
    "Bruise": {3: ["Pot Twoday"]},
    "Candy": {2: ["Root"], 3: ["Root"], 4: ["Root"], 11: ["Sprinkling"]},
    "Fever": {},
    "Junuary": {17: ["Croppaday"]},
    "Septa": {1: ["Belabor Day"]},
    "Remember": {
        25: ["Zilloween"],
        26: ["Zilloween"],
        27: ["Zilloween"],
        28: ["Zilloween"],
        29: ["Zilloween"],
        30: ["Zilloween"],
        31: ["Zilloween"],
        32: ["Zilloween"],
        33: ["Zilloween"],
        34: ["Zilloween"],
        35: ["Zilloween"],
        36: ["Zilloween"],
        37: ["Zilloween"],
    },
    "Doom": {},
    "Widdershins": {},
    "Eleventy": {11: ["Recurse Eve"]},
    "Recurse": {1: ["Recurse"]},
}


def epoch_seconds(dt: datetime) -> int:
    return (dt - EPOCH).total_seconds()


def utc_time():
    return datetime.datetime.now(tz=TZ_UTC)


@time_blueprint.route("/time/utc")
def _utc_time():
    return Response(str(utc_time()), mimetype="text/plain")


@time_blueprint.route("/getHolidays")
def _get_holidays():
    month = request.args.get("month", type=int)
    day = request.args.get("day", type=int)

    if month > len(MONTH_NAMES) - 1:
        return jsonify(list())

    month_name = MONTH_NAMES[month]

    if month_name not in HOLIDAYS_BY_MONTH:
        return jsonify(list())

    holidays_for_month = HOLIDAYS_BY_MONTH[month_name]

    if day not in holidays_for_month.keys():
        return jsonify(list())

    return jsonify(holidays_for_month[day])


class Clock:
    """
    day: ordinal of month: '13th'
    dayInt: day of month, starting at 1: 17
    dayofweek: name of day: 'Twoday'
    month: name of month: 'Primuary'
    monthInt: number of month, starting at 1: 6
    time: formatted time: '6:23pm'
    year: name of year: 'Year 37'
    """

    _day_of_week: str
    _year: str
    _day: str
    _month: str
    _time: str

    _day_int: int
    _month_int: int

    @property
    def day_of_week(self) -> str:
        return self._day_of_week

    @property
    def year(self) -> str:
        return self._year

    @property
    def day(self) -> str:
        return self._day

    @property
    def month(self) -> str:
        return self._month

    @property
    def time(self) -> str:
        return self._time

    @property
    def day_int(self) -> int:
        return self._day_int

    @property
    def month_int(self) -> int:
        return self._month_int

    @property
    def hour_int(self) -> int:
        if "am" in self.time:
            return int(self.time.replace("am", "").split(":")[0])
        else:
            return int(self.time.replace("pm", "").split(":")[1]) + 12

    start_date: datetime

    def __init__(self, dt: datetime = None):
        self.start_date = dt if dt else datetime.datetime.now()

        if dt:
            self._init_data()

    def _init_data(self):
        data = self.date
        self._day_of_week = data[3]
        self._time = data[4]
        self._day = data[2]
        self._month = data[1]
        self._year = data[0]
        self._day_int = data[5]
        self._month_int = data[6]

    def _send_events(self):
        self._init_data()
        # TODO: _timeupdateController.add([time, day, dayofweek, month, year, dayInt, monthInt])

        if self.time == "6:00am":
            # TODO: _newdayController.add("new day!")
            pass

    @property
    def date(self) -> list:
        """
        There are 4435200 real seconds in a game year.
		There are 14400 real seconds in a game day.
		There are 600 real seconds in a game hour.
		There are 10 real seconds in a game minute.
        """
        # How many real seconds have elapsed since game epoch?
        date = self.start_date
        ts = epoch_seconds(date)
        sec = ts - 1_238_562_000

        year = sec // 4_435_200
        sec -= year * 4_435_200

        day_of_year = sec // 14400
        sec -= day_of_year * 14400

        hour = sec // 600
        sec -= hour * 600

        minute = sec // 10
        sec -= minute * 10

        # Turn the 0-based day-of-year into a day and month
        month, day = Clock._doy_to_mad(day_of_year)

        # Get day-of-week
        days_since_epoch = day_of_year + (307 * year)
        day_of_week = days_since_epoch % 8

        # Fix AM/PM times
        h = str(hour)
        m = str(minute)
        am_pm = "pm" if hour >= 12 else "am"

        if minute < 10:
            m = f"0{minute}"

        if hour > 12:
            h = str(hour - 12)

        current_time = f"{h}:{m}{am_pm}"

        return [
            f"Year {year}",
            MONTH_NAMES[month],
            str(day) + Clock._suffix(day),
            DAY_NAMES[day_of_week],
            current_time,
            day,
            month,
        ]

    @staticmethod
    def _suffix(ordinal: int) -> str:
        return {"1": "st", "2": "nd", "3": "rd"}.get(str(ordinal)[-1], default="th")

    @staticmethod
    def _doy_to_mad(day_of_year: int) -> (int, int):
        """
        Convert day of year to month and day.

        :returns: month, day
        """
        cd = 0
        days_in_months = sum(DAYS_PER_MONTH)

        for i in range(days_in_months):
            cd += DAYS_PER_MONTH[i]

            if cd > day_of_year:
                return i, day_of_year + 1 - (cd - DAYS_PER_MONTH[i])

        return 0, 0

    def __str__(self):
        return f"{self.day}{Clock._suffix(self.day_int)} of {MONTH_NAMES[self.month_int]}, {self.year}"
