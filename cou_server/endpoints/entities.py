from flask import Blueprint, jsonify, request, Response
from flask_cors import CORS
import inflect

from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.endpoints.mapdata import tsid_l
from cou_server.models.entity import Entity

logger = get_logger(__name__)
inflector = inflect.engine()

entity_blueprint = Blueprint("entity_blueprint", __name__)
CORS(entity_blueprint, automatic_options=True)


@entity_blueprint.route("/previewStreetEntities")
def preview_street_entities():
    tsid = request.args.get("tsid", type=str)
    if not tsid:
        return Response("No tsid provided", status=400)

    tsid = tsid_l(tsid)
    num_entities = {}
    quoin_types = [
        "Img",
        "Mood",
        "Currant",
        "Mystery",
        "Favor",
        "Energy",
        "Time",
        "Quarazy",
    ]
    with db_session_context() as db_session:
        entities = (
            db_session.query(Entity)
            .filter(Entity.tsid == tsid)
            .filter(Entity.type.notin_(quoin_types))
            .all()
        )

        for entity in entities:
            # change a string like GardeningGoodsVendor to Gardening Goods Vendor
            new_type = "".join(
                map(lambda x: x if x.islower() else f" {x}", entity.type)
            ).strip()

            # if new_type.lower() in StreetSpiritGroddle.VARIANTS:
            # new_type = f'Shrine to {new_type}'

            if "Street Spirit" in new_type:
                new_type = "Street Spirit"

            if new_type in num_entities:
                num_entities[new_type] += 1
            else:
                num_entities[new_type] = 1

        pluralized_entities = {}
        for type, count in num_entities.items():
            if count > 1:
                pluralized_entities[inflector.plural(type)] = count
            else:
                pluralized_entities[type] = count

    return jsonify(pluralized_entities)
