import json

from importlib import resources

from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.message_bus import MessageBus
from cou_server.models.user import User

from flask import Blueprint, Response, jsonify, request
from flask_cors import CORS

quest_blueprint = Blueprint("quest_blueprint", __name__, url_prefix="/quest")
quests_blueprint = Blueprint("quests_blueprint", __name__, url_prefix="/quests")
quests_ws_blueprint = Blueprint("quests_ws_blueprint", __name__, url_prefix="/quest")
CORS(quests_blueprint, automatic_options=True)

logger = get_logger(__name__)
user_sockets = {}
quests = {}
quest_log_cache = {}


def update_quest_log(quest_log):
    pass


@quests_blueprint.route("/requirementTypes")
def requirementTypes():
    types = []
    events = []

    for quest_id, quest in quests.items():
        for requirement in quest.requirements:
            if requirement.type not in types:
                types.append(requirement.type)
            if requirement.event_type not in events:
                events.append(requirement.event_type)

    return {"types": types, "events": events}


@quest_blueprint.route("/pieces")
def get_pieces():
    pieces = {
        "getItem_<Item>": "Aquire Item",
        "makeRecipe_<Item>": "Make Recipe",
        "treePet<Tree>": "Pet Tree",
        "location_<Location>": "Goto Location",
        "sendMail_<Player>_containingItems_<List_Item>_currants_<int>": "Send Mail",
    }

    return jsonify(pieces)


def cleanup_list(socket):
    leaving_user = None

    for email, ws in user_sockets.items():
        if ws == socket:
            ws = None
            leaving_user = email

    if leaving_user and leaving_user in quest_log_cache:
        quest_log_cache[leaving_user].stop_tracking()
        quest_log_cache.pop(leaving_user)
        user_sockets.pop(leaving_user)


@quests_ws_blueprint.route("")
def quests_ws(socket):
    from cou_server.quests import messages
    from cou_server.quests.quest_log import QuestLog

    while not socket.closed:
        try:
            data = json.loads(socket.receive())
        except Exception as e:
            break

        if data.get("connect", None):
            email = data["email"]

            # setup our associative data structures
            user_sockets[email] = socket

            # TODO: persist the quest stuff in the db
            quest_log_cache[email] = QuestLog()
            # quest_log_cache[email] = await QuestService.getQuestLog(email);

            # start tracking this user's quest log
            quest_log_cache[email].start_tracking(email)

        if data.get("acceptQuest", None):
            try:
                MessageBus().publish(
                    "accept_quest", messages.AcceptQuest(data["id"], data["email"])
                )
            except:
                logger.exception(
                    f"Accepting quest <id={data.get('id')}> for <email={data['email']}>"
                )

        if data.get("rejectQuest", None):
            try:
                MessageBus().publish(
                    "reject_quest", messages.RejectQuest(data["id"], data["email"])
                )
            except:
                logger.exception(
                    f"Rejecting quest <id={data.get('id')}> for <email={data['email']}>"
                )

    cleanup_list(socket)


def load_quests():
    from cou_server.quests.quest import Quest, QuestSchema

    try:
        for quest in [
            q for q in resources.contents("data.quests") if q.endswith(".json")
        ]:
            with resources.open_text("data.quests", quest) as quest_file:
                quest_data = json.loads(quest_file.read())
                quests[quest_data["id"]] = QuestSchema().load(quest_data)

        logger.debug(f"Loaded {len(quests)} quests")
    except:
        logger.exception(f"Could not load quest data for {quest}")
