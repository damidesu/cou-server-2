from importlib import resources
import json
from pathlib import Path

from flask import Blueprint, jsonify, request
from flask_cors import CORS

from cou_server.common import get_logger
from cou_server.entities.items.item import Item


items_endpoint = Blueprint("items_endpoint", __name__)
CORS(items_endpoint, automatic_options=True)

logger = get_logger(__name__)
items = {}


@items_endpoint.route("/getItems")
def items_get():
    name = request.args.get("name", default=None)
    type = request.args.get("type", default=None)
    isRegex = request.args.get("isREgex", default=False)

    return jsonify([])


def load_items():
    for item_file in [
        i for i in resources.contents("data.items") if i.endswith(".json")
    ]:
        with resources.open_text("data.items", item_file) as item_file:
            for name, item_map in json.loads(item_file.read()).items():
                item_map["itemType"] = name
                items[name] = Item(**item_map)
    logger.debug(f"[Item] loaded {len(items)} items")
    return len(items)
