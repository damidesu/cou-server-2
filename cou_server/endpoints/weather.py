import json, urllib.request

from datetime import datetime, timedelta
from numbers import Number

from flask import Blueprint
from geventwebsocket.websocket import WebSocket

from cou_server.api_keys import OWM_API_KEY
from cou_server.common import TZ_UTC, get_logger
from cou_server.endpoints.mapdata import get_street_by_tsid, hubs

weather_ws_blueprint = Blueprint(
    "weather_ws_blueprint", __name__, url_prefix="/weather"
)


logger = get_logger(__name__)

_OWM_API = "http://api.openweathermap.org/data/2.5/"
_OWM_PARAMS = f"?appid={OWM_API_KEY}&id="

_WEATHER_CACHE = dict()
_WEATHER_SOCKETS = dict()
_deferred_minutes = 0


class WeatherData:
    _OWM_IMG = "https://openweathermap.org/img/w/"

    @staticmethod
    def k_to_f(kelvin: float) -> float:
        """Convert a temperature in Kelvin to degrees Fahrenheit."""
        return (kelvin * (9 / 5)) - 459.67

    def __init__(self, owm: dict):
        self.city_id = owm.get("id")
        self.city_name = owm.get("name")

        self.weather_id = owm["weather"][0]["id"]
        self.weather_main = owm["weather"][0]["main"]
        self.weather_description = owm["weather"][0]["description"]
        self.weather_icon = self._OWM_IMG + owm["weather"][0]["icon"] + ".png"

        if "coord" in owm:
            self.longitude = owm["coord"]["lon"]
            self.latitude = owm["coord"]["lat"]

        if "main" in owm:
            self.temp = WeatherData.k_to_f(owm["main"]["temp"])
            self.temp_min = WeatherData.k_to_f(owm["main"]["temp_min"])
            self.temp_max = WeatherData.k_to_f(owm["main"]["temp_max"])
        elif "temp" in owm:
            self.temp_min = WeatherData.k_to_f(owm["temp"]["min"])
            self.temp_max = WeatherData.k_to_f(owm["temp"]["max"])
            self.temp = (self.temp_min + self.temp_max) / 2
            self.temp_day = WeatherData.k_to_f(owm["temp"]["day"])
            self.temp_night = WeatherData.k_to_f(owm["temp"]["night"])
            self.temp_eve = WeatherData.k_to_f(owm["temp"]["eve"])
            self.temp_morn = WeatherData.k_to_f(owm["temp"]["morn"])

        self.humidity = (
            owm["humidity"] if "humidity" in owm else owm["main"]["humidity"]
        )

        self.pressure = (
            owm["pressure"] if "pressure" in owm else owm["main"]["pressure"]
        )
        self.sea_level_pressure = owm.get("main", {}).get("sea_level", 0)
        self.ground_level_pressure = owm.get("main", {}).get("ground_level", 0)

        self.wind_speed = owm.get("wind", {}).get("speed", 0)
        self.wind_deg = owm.get("wind", {}).get("angle", 0)

        if "clouds" in owm and isinstance(owm["clouds"], Number):
            self.clouds = owm["clouds"]
        elif "clouds" in owm and isinstance(owm["clouds"], dict):
            self.clouds = owm["clouds"].get("3h", 0)
        else:
            self.clouds = 0

        if "rain" in owm and isinstance(owm["rain"], Number):
            self.rain_vol = owm["rain"]
        elif "rain" in owm and isinstance(owm["rain"], dict):
            self.rain_vol = owm["rain"].get("3h", 0)
        else:
            self.rain_vol = 0

        if "snow" in owm and isinstance(owm["snow"], Number):
            self.snow_vol = owm["snow"]
        elif "snow" in owm and isinstance(owm["snow"], dict):
            self.snow_vol = owm["snow"].get("3h", 0)
        else:
            self.snow_vol = 0

        self.calc_date = datetime.fromtimestamp(owm["dt"], TZ_UTC)

        if "sys" in owm:
            self.country_code = owm["sys"]["country"]

            if "sunrise" in owm["sys"]:
                self.sunrise = datetime.fromtimestamp(owm["sys"]["sunrise"], TZ_UTC)
                pass

            if "sunset" in owm["sys"]:
                self.sunset = datetime.fromtimestamp(owm["sys"]["sunset"], TZ_UTC)
                pass

    def to_dict(self):
        return {
            "latitude": getattr(self, "latitude", None),
            "longitude": getattr(self, "longitude", None),
            "weatherId": getattr(self, "weather_id", None),
            "weatherMain": getattr(self, "weather_main", None),
            "weatherDesc": getattr(self, "weather_description", None),
            "weatherIcon": getattr(self, "weather_icon", None),
            "temp": getattr(self, "temp", None),
            "tempMin": getattr(self, "temp_min", None),
            "tempMax": getattr(self, "temp_max", None),
            "tempDay": getattr(self, "temp_day", None),
            "tempNight": getattr(self, "temp_night", None),
            "tempEve": getattr(self, "temp_eve", None),
            "tempMorn": getattr(self, "temp_morn", None),
            "humidity": getattr(self, "humidity", None),
            "pressure": getattr(self, "pressure", None),
            "seaLevelPressure": getattr(self, "sea_level_pressure", None),
            "groundLevelPressure": getattr(self, "ground_level_pressure", None),
            "windSpeed": getattr(self, "wind_speed", None),
            "windDeg": getattr(self, "wind_deg", None),
            "clouds": getattr(self, "clouds", None),
            "rainVol": getattr(self, "rain_vol", None),
            "snowVol": getattr(self, "snow_vol", None),
            "calcDateTxt": str(getattr(self, "calc_date", None)),
            "countryCode": getattr(self, "country_code", None),
            "sunriseTxt": str(getattr(self, "sunrise", None)),
            "sunsetTxt": str(getattr(self, "sunset", None)),
            "cityId": getattr(self, "city_id", None),
            "cityName": getattr(self, "city_name", None),
        }


class WeatherLocation:
    _MAX_AGE = timedelta(hours=1)

    def __init__(self, current: WeatherData, forecast: list, dt: datetime = None):
        self.current = current
        self.forecast = forecast
        self.datetime = dt if dt else datetime.now(TZ_UTC)

    @property
    def expired(self) -> bool:
        return datetime.now(TZ_UTC) > self.datetime + self._MAX_AGE

    def to_dict(self):
        return {
            "current": self.current.to_dict(),
            "forecast": [f.to_dict() for f in self.forecast],
        }


def get_conditions(tsid: str, using_hub_id: bool = False) -> WeatherData or None:
    """Return the weather data for a TSID, or None if the TSID does not have weather."""
    city_id = get_city_id(tsid, using_hub_id)

    if not city_id:
        return None

    if city_id in _WEATHER_CACHE and not _WEATHER_CACHE[city_id].expired:
        return _WEATHER_CACHE[city_id]

    return download(city_id)


def get_conditions_dict(tsid: str, using_hub_id: bool = False) -> dict:
    """Return the weather data for a TSID as a dict."""
    weather = get_conditions(tsid, using_hub_id)
    if weather:
        return weather.to_dict()
    else:
        return {"error": "no_weather", "hub_id" if using_hub_id else "tsid": tsid}


def get_city_id(tsid, using_hub_id: bool = False) -> int or None:
    """Return the city id for a TSID or None if it doesn't have one."""

    if not using_hub_id:
        street = get_street_by_tsid(tsid)

        street_city_id = street.get("owm_city_id", None)
        if street_city_id:
            return street_city_id

    # check hub
    hub_id = tsid if using_hub_id else street["hub_id"]
    hub = hubs()[str(hub_id)]
    return hub.get("owm_city_id", None)


@weather_ws_blueprint.route("")
def handle(socket: WebSocket):
    """Add a new client."""

    while not socket.closed:
        try:
            data = json.loads(socket.receive())
        except Exception as e:
            break

        _process_message(socket, data)

    _cleanup_list(socket)


def _process_message(web_socket: WebSocket, msg_json: dict):
    """Handle incoming messages from clients."""
    username = msg_json["username"]
    tsid = msg_json["tsid"]
    hub = msg_json.get("hub", None)

    # Add reference to user if not already stored
    if username not in _WEATHER_SOCKETS:
        _WEATHER_SOCKETS[username] = web_socket

    # Send the current weather to the just connected user
    if tsid:
        # Get weather data for location
        web_socket.send(json.dumps(get_conditions_dict(tsid)))
    elif hub:
        web_socket.send(
            json.dumps(
                {
                    "conditions": get_conditions_dict(hub, using_hub_id=True),
                    "hub_id": hub,
                    "hub_label": hubs()[hub]["name"],
                }
            )
        )
    else:
        # Client will retry when it is done loading
        web_socket.send("Street not loaded")
        web_socket.close()


def _cleanup_list(web_socket: WebSocket):
    """Remove disconnected clients."""
    leaving_user: str = None

    for username, socket in _WEATHER_SOCKETS.items():
        if web_socket == socket:
            _WEATHER_SOCKETS[username] = None
            leaving_user = username

    del _WEATHER_SOCKETS[leaving_user]


def _owm_download(endpoint: str, dl_city_id: int):
    url = _OWM_API + endpoint + _OWM_PARAMS + str(dl_city_id)
    resource = urllib.request.Request(url)
    data = urllib.request.urlopen(resource).read().decode()
    owm = json.loads(data)
    response_code = int(owm["cod"])  # 'cod' is not a typo (unless it's OWM's)

    if response_code != 200:
        raise ValueError(f"OWM API call returned {response_code} for {url}")

    return owm


def download(city_id: int = None) -> WeatherData or None:
    """
    Update cached weather data from OpenWeatherMap.

    Pass cityId to download for one city (and return the data),
    or not to refresh the entire cache (and return true).
    """
    global _deferred_minutes, _last_check

    # Waiting for recovery?
    if _deferred_minutes > 0:
        if not _last_check:
            _last_check = datetime.now(tz=TZ_UTC)

        # Decrement each minute
        if _last_check.minute < datetime.now(tz=TZ_UTC).minute:
            _deferred_minutes -= 1

        return None
    else:
        _last_check = datetime.now(tz=TZ_UTC)

    if not city_id:
        for cached_city_id in _WEATHER_CACHE.keys():
            download(cached_city_id)
    else:
        try:
            # Get current conditions
            current = WeatherData(_owm_download("weather", city_id))

            # Get forecast conditions
            forecast = list()
            days = _owm_download("forecast/daily", city_id)["list"]
            for day in days[1:5]:
                forecast.append(WeatherData(day))

            # Assemble location data
            weather = WeatherLocation(current, forecast)

            # Add to cache
            _WEATHER_CACHE[city_id] = weather

            return weather
        except Exception as e:
            logger.exception(f"Error downloading weather for city id '{city_id}'.")
            _deferred_minutes += 1
            return None


def is_raining_in(tsid: str) -> bool:
    """Whether the weather is rainy on a given street."""
    weather = get_conditions(tsid)

    if not weather:
        return False

    return (
        "rain" in weather.current.weather_main.lower()
        or "rain" in weather.current.weather_description.lower()
    )


# TODO: call every 5 seconds
def update_all_clients():
    """Send data to all clients."""
    for username in _WEATHER_SOCKETS:
        tsid = str()  # TODO: PlayerUpdateHandler.users[username].tsid

        _WEATHER_SOCKETS[username].send(
            json.dumps({"current": get_conditions_dict(tsid), "tsid": tsid})
        )
