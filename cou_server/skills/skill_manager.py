from importlib import resources
import json

from flask import Blueprint, jsonify, request
from flask_cors import CORS

from cou_server import api_keys
from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.util import toast
from cou_server.models.user import User
from cou_server.skills.player_skill import PlayerSkill
from cou_server.skills.skill import Skill


logger = get_logger(__name__)


logger = get_logger(__name__)
skills_blueprint = Blueprint("skills_blueprint", __name__, url_prefix="/skills")
CORS(skills_blueprint, automatic_options=True)

# All loaded skills...do not edit after initial load!
skills = {}


def load_skills():
    for skill_file in [
        i for i in resources.contents("data.skills") if i.endswith(".json")
    ]:
        with resources.open_text("data.skills", skill_file) as skill_file:
            for name, skill_map in json.loads(skill_file.read()).items():
                skills[name] = Skill(**skill_map)
    logger.debug(f"[Skills] loaded {len(skills)} skills")
    return len(skills)


class SkillManager:
    @staticmethod
    def learn(skill_id, email, new_points=1):
        from cou_server.endpoints.street_update import user_sockets

        # get existing skill or add if new
        skill = PlayerSkill.find(skill_id, email)
        if skill.points == 0:
            toast(
                f"You've started learning {skill.name}!",
                user_sockets[email],
                on_click="imgmenu",
            )

        # save to database
        success = skill.add_points(new_points)

        if success["level_up"]:
            toast(
                f"Your {skill.name} skill is now at level {skill.level}",
                user_sockets[email],
                on_click="imgmenu",
            )

        return success["writing"]

    @staticmethod
    def get_level(skill_id, email):
        skill = PlayerSkill.find(skill_id, email)
        if skill and skill.level:
            return skill.level
        return 0

    @staticmethod
    def get_player_skills(email=None, username=None):
        if email is None and username is None:
            return None

        try:
            with db_session_context() as db_session:
                if email is None and username != None:
                    user = (
                        db_session.query(User).filter(User.username == username).first()
                    )
                else:
                    user = db_session.query(User).filter(User.email == email).first()
                player_skills_json = user.metabolics.skills_json

                # fill in skill information
                player_skills_list = []
                for id, points in player_skills_json.items():
                    player_skills_list.append(
                        PlayerSkill(Skill.find(id), email, points).to_map()
                    )

                return player_skills_list
        except Exception:
            logger.exception(f"Error getting skill list for email <email={email}>")


@skills_blueprint.route("/get/<string:email>")
def skills_get(email):
    return jsonify(SkillManager.get_player_skills(email=email))


@skills_blueprint.route("/getByUsername/:username")
def get_player_skills_username_route(username):
    return jsonify(SkillManager.get_player_skills(username=username))


cached_data = None


@skills_blueprint.route("/list")
def all_skills():
    global cached_data
    token = request.args.get("token")
    if token != api_keys.REDSTONE_TOKEN:
        return jsonify([{"error": "true"}, {"token": "invalid"}])

    if cached_data != None:
        return cached_data
    else:
        result = []
        for skill in skills.values():
            result.append(skill.to_map())
        cached_data = result
        return result
