from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.util import clamp
from cou_server.models.user import User


logger = get_logger(__name__)


class Skill:
    """Stores information about a specific skill
    Instances should not be created/modified/destroyed after the server has initially loaded
    """

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            try:
                setattr(self, key, value)
            except Exception:
                # skip the unsettable properties
                pass
        self.id = self.name.lower()
        self._verify_zero_state()

    @staticmethod
    def find(id):
        from cou_server.skills.skill_manager import skills

        return Skill(**skills[id].to_map())

    def to_map(self):
        return {
            "id": self.id,
            "name": self.name,
            "category": self.category,
            "descriptions": self.descriptions,
            "levels": self.levels,
            "num_levels": self.num_levels,
            "iconUrls": self.iconUrls,
            "requirements": self.requirements,
            "giants": self.giants,
            "primary_giant": self.primary_giant,
            "color": self.color,
            "title": self.title,
        }

    def __str__(self):
        return f"<Skill {self.id}>"

    @property
    def num_levels(self):
        return len(self.levels) - 1

    def _verify_zero_state(self):
        if self.levels[0] != 0:
            self.levels.insert(0, 0)

        if len(self.levels) - 1 != len(self.descriptions):
            raise ValueError(
                f"Levels and descriptions for <Skill={self.id}> do not match!"
            )

        if len(self.levels) - 1 != len(self.iconUrls):
            raise ValueError(f"Levels and icons for <Skill={self.id}> do not match!")

    def points_for_level(self, level):
        """x points yields y level"""
        level = clamp(level, 0, self.num_levels)
        return self.levels[level]

    def level_for_points(self, points):
        """x level requires y points"""
        for level in range(0, self.num_levels):
            if self.points_for_level(level) > points:
                # this level requires more points than we have, go back one
                return clamp(level - 1, 0, self.num_levels)

        # points maxed out
        return self.num_levels

    @property
    def primary_giant(self):
        """Primary giant affiliation (first in file)"""
        return self.giants[0]

    def get_for_player(self, email):
        from cou_server.skills.player_skill import PlayerSkill

        try:
            with db_session_context() as db_session:
                m = (
                    db_session.query(User)
                    .filter(User.email == email)
                    .first()
                    .metabolics
                )
                player_skill = m.skills_json
                points = player_skill.get(self.id, 0)
                return PlayerSkill(Skill(**self.to_map()), email, points)
        except Exception:
            logger.exception(f"Error getting skill {self.id}")
            return None
