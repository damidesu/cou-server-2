from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.models.user import User
from cou_server.skills.skill import Skill


logger = get_logger(__name__)


class PlayerSkill(Skill):
    """PlayerSkill class, used to represent a single player's status on a skill.
    Basically a Skill with stored player data.
    """

    def __init__(self, base, email, points=None):
        super().__init__(**base.to_map())
        self.email = email
        self.points = points

    def __str__(self):
        return f"<Skill {self.id} for <email={self.email}>>"

    @staticmethod
    def find(skill_id, email):
        return Skill.find(skill_id).get_for_player(email)

    def to_map(self):
        base = super().to_map()
        base.update(
            {
                "player_email": self.email,
                "player_points": self.points,
                "player_nextPoints": self.points_for_level(self.level + 1),
                "player_level": self.level,
                "player_iconUrl": self.icon_url,
                "player_description": self.description,
            }
        )
        return base

    @property
    def level(self):
        return self.level_for_points(self.points)

    def add_points(self, new_points):
        old_level = self.level
        self.points += new_points
        success = self._write()
        return {"writing": success, "level_up": (self.level > old_level)}

    @property
    def icon_url(self):
        if self.level > 0:
            return self.iconUrls[self.level - 1]
        return "https://childrenofur.com/assets/skillNotYetLearned.png"

    @property
    def description(self):
        if self.level > 0:
            return self.descriptions[self.level - 1]
        return f"You're still learning the basics of {self.name}"

    def _write(self):
        try:
            with db_session_context() as db_session:
                m = (
                    db_session.query(User)
                    .filter(User.email == self.email)
                    .first()
                    .metabolics
                )
                m.skills_json[self.id] = self.points
        except Exception:
            logger.exception(f"Error setting skill {self.id} for <email={self.email}>")
