class RequirementMessage:
    def __init__(self, requirement, email):
        self.requirement = requirement
        self.email = email


class CompleteRequirement(RequirementMessage):
    pass


class FailRequirement(RequirementMessage):
    pass


class QuestFinishMessage:
    def __init__(self, quest, email):
        self.quest = quest
        self.email = email


class CompleteQuest(QuestFinishMessage):
    pass


class FailQuest(QuestFinishMessage):
    pass


class QuestStartMessage:
    def __init__(self, quest_id, email):
        self.quest_id = quest_id
        self.email = email


class AcceptQuest(QuestStartMessage):
    pass


class RejectQuest(QuestStartMessage):
    pass


class RequirementProgress:
    def __init__(self, event_type, email, count=1):
        self.event_type = event_type
        self.email = email
        self.count = count


class RequirementUpdated:
    def __init__(self, requirement, email):
        self.requirement = requirement
        self.email = email


class PlayerPosition:
    def __init__(self, street_name, email, x, y):
        self.street_name = street_name
        self.email = email
        self.x = x
        self.y = y
