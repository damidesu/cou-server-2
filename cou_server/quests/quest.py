import json

from marshmallow import fields, post_load

from cou_server.common.message_bus import MessageBus, subscribe
from cou_server.endpoints.quest import user_sockets, quests
from cou_server.quests import NotNullSchema, messages
from cou_server.quests.requirement import Requirement, RequirementSchema
from cou_server.quests.trackable import Trackable


class Conversation:
    def __init__(self):
        self.id = None
        self.title = None
        self.screens = []


class ConvoScreen:
    def __init__(self):
        self.paragraphs = []
        self.choices = []


class ConvoChoice:
    def __init__(self):
        self.text = None
        self.goto_screen = None
        self.is_quest_accept = False
        self.is_quest_reject = False


class QuestRewards:
    def __init__(self):
        self.energy = 0
        self.mood = 0
        self.img = 0
        self.currants = 0
        self.favor = []


class QuestFavor:
    def __init__(self):
        self.giant_name = None
        self.fav_amt = None


class Quest(Trackable):
    def __init__(self, **kwargs):
        self.id = None
        self.title = None
        self.description = None
        self.complete = False
        self.prerequisites = []
        self.requirements = []
        self.conversation_start = None
        self.conversation_end = None
        self.conversation_fail = None
        self.rewards = None

        for prop in kwargs:
            setattr(self, prop, kwargs[prop])

        Trackable.__init__(self)

        self.mb_subscriptions.append(
            subscribe("complete_requirement", method=self.handle_complete_requirement)
        )
        self.mb_subscriptions.append(
            subscribe("fail_requirement", method=self.handle_fail_requirement)
        )
        self.mb_subscriptions.append(
            subscribe("requirement_updated", method=self.handle_requirement_updated)
        )

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    @staticmethod
    def clone(quest_id):
        cloned = Quest()
        model = quests[quest_id]

        cloned.id = model.id
        cloned.title = model.title
        cloned.description = model.description
        cloned.prerequisites = model.prerequisites
        cloned.requirements = [Requirement.clone(r) for r in model.requirements]
        cloned.conversation_start = model.conversation_start
        cloned.conversation_end = model.conversation_end
        cloned.conversation_fail = model.conversation_fail
        cloned.rewards = model.rewards

        return cloned

    def handle_complete_requirement(self, data):
        if data.requirement not in self.requirements or data.email != self.email:
            return

        try:
            next(r for r in self.requirements if not r.fulfilled)
        except StopIteration:
            self.complete = True
            MessageBus().publish(
                "complete_quest", messages.CompleteQuest(self, self.email)
            )
            self._give_rewards()

    def handle_fail_requirement(self, data):
        if data.requirement not in self.requirements or data.email != self.email:
            return

        MessageBus().publish("fail_quest", messages.FailQuest(self, self.email))
        self.stop_tracking()

    def handle_requirement_updated(self, data):
        if data.requirement not in self.requirements or data.email != self.email:
            return

        user_socket = user_sockets.get(self.email)
        if not user_socket:
            return
        update_message = {"questUpdate": True, "quest": QuestSchema().dump(self)}
        user_socket.send(json.dumps(update_message))

    def start_tracking(self, email, just_started=False):
        if self.complete:
            return

        super().start_tracking(email)

        for requirement in self.requirements:
            requirement.start_tracking(email)

        heading = "questBegin" if just_started else "questInProgress"
        quest_in_progress = {heading: True, "quest": QuestSchema().dump(self)}
        user_socket = user_sockets.get(self.email)
        if user_socket:
            user_socket.send(json.dumps(quest_in_progress))

    def _give_rewards(self):
        # TODO
        pass
        # return trySetMetabolics(email, rewards: rewards);

    def stop_tracking(self):
        super().stop_tracking()
        for requirement in self.requirements:
            requirement.stop_tracking()


class ConvoChoiceSchema(NotNullSchema):
    text = fields.Str()
    goto_screen = fields.Int(data_key="gotoScreen")
    is_quest_accept = fields.Bool(data_key="isQuestAccept", default=False)
    is_quest_reject = fields.Bool(data_key="isQuestReject", default=False)


class ConvoScreenSchema(NotNullSchema):
    paragraphs = fields.List(fields.Str())
    choices = fields.List(fields.Nested(ConvoChoiceSchema))


class ConversationSchema(NotNullSchema):
    id = fields.Str()
    title = fields.Str()
    screens = fields.List(fields.Nested(ConvoScreenSchema))


class QuestFavorSchema(NotNullSchema):
    giant_name = fields.Str(data_key="giantName")
    fav_amt = fields.Int(data_key="favAmt")


class QuestRewardsSchema(NotNullSchema):
    energy = fields.Int(default=0)
    mood = fields.Int(default=0)
    img = fields.Int(default=0)
    currants = fields.Int(default=0)
    favor = fields.List(fields.Nested(QuestFavorSchema), default=[])


class QuestSchema(NotNullSchema):
    complete = fields.Bool(default=False)
    conversation_end = fields.Nested(ConversationSchema)
    conversation_start = fields.Nested(ConversationSchema)
    conversation_fail = fields.Nested(ConversationSchema)
    description = fields.Str()
    id = fields.Str()
    prerequisites = fields.List(fields.Str(), default=[])
    requirements = fields.List(fields.Nested(RequirementSchema), default=[])
    rewards = fields.Nested(QuestRewardsSchema)
    title = fields.Str()

    @post_load
    def make_quest(self, data, **kwargs):
        return Quest(**data)
