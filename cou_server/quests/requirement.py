import re
from threading import Timer

from marshmallow import fields, post_load

from cou_server.quests import NotNullSchema, messages
from cou_server.quests.trackable import Trackable
from cou_server.common.message_bus import MessageBus, subscribe


class Requirement(Trackable):
    def __init__(self, **kwargs):
        super().__init__()
        self._fullfilled = False
        self.failed = False
        self._num_fullfilled = 0
        self.num_required = None
        self.time_limit = None
        self.id = None
        self.text = None
        self.req_type = None
        self.event_type = None
        self.icon_url = ""
        self.req_type_done = []

        for prop in kwargs:
            setattr(self, prop, kwargs[prop])

        Trackable.__init__(self)

        self.mb_subscriptions.append(
            subscribe("requirement_progress", method=self.handle_requirement_progress)
        )

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    def __repr__(self):
        return f"Requirement(id={self.id}, event_type={self.event_type})"

    @staticmethod
    def clone(model):
        requirement = Requirement()
        requirement.num_required = model.num_required
        requirement.time_limit = model.time_limit
        requirement.id = model.id
        requirement.text = model.text
        requirement.req_type = model.req_type
        requirement.event_type = model.event_type
        requirement.icon_url = model.icon_url
        return requirement

    @property
    def fulfilled(self):
        return self._fullfilled

    @fulfilled.setter
    def fulfilled(self, new_val):
        self._fullfilled = new_val
        if self._fullfilled and self.being_tracked:
            MessageBus().publish(
                "complete_requirement", messages.CompleteRequirement(self, self.email)
            )

    @property
    def num_fullfilled(self):
        return self._num_fullfilled

    @num_fullfilled.setter
    def num_fullfilled(self, new_val):
        self._num_fullfilled = new_val
        if self._num_fullfilled == self.num_required:
            self.fulfilled = True

    def handle_requirement_progress(self, progress):
        if progress.email != self.email:
            return

        good_event = False
        count = 1
        if self._matching_event(progress.event_type):
            if (
                self.req_type == "counter_unique"
                and progress.event_type not in self.req_type_done
            ):
                good_event = True
                self.req_type_done.append(progress.event_type)
            elif self.req_type == "counter" or self.req_type == "timed":
                good_event = True
                count = progress.count

        if not good_event or self.fulfilled:
            return

        self.num_fullfilled += count
        MessageBus().publish(
            "requirement_updated", messages.RequirementUpdated(self, self.email)
        )

    def start_tracking(self, email):
        if self.fulfilled:
            return

        super().start_tracking(email)

        if self.req_type == "timed":

            def fail_time_limit():
                MessageBus().publish(
                    "fail_requirement", messages.FailRequirement(self, email)
                )

            self.limit_timer = Timer(self.time_limit, fail_time_limit)

    def _matching_event(self, event):
        if re.search(self.event_type, event):
            return True
        return False


class RequirementSchema(NotNullSchema):
    event_type = fields.Str(data_key="eventType")
    fulfilled = fields.Bool(default=False)
    icon_url = fields.Str(data_key="iconUrl", default="")
    id = fields.Str()
    num_fullfilled = fields.Int(data_key="numFulfilled", default=0)
    num_required = fields.Int(data_key="numRequired")
    text = fields.Str()
    time_limit = fields.Int(data_key="timeLimit")
    req_type = fields.Str(data_key="type")
    req_type_done = fields.List(fields.Str(), data_key="typeDone", default=[])

    @post_load
    def make_requirement(self, data, **kwargs):
        return Requirement(**data)
