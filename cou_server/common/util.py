import json
import time
from typing import Callable, Union

from geventwebsocket.websocket import WebSocket

MIN_CLIENT_VERSION = 1471


prompt_callbacks = {}


def prompt_string(
    prompt: str,
    user_socket: WebSocket,
    reference: str,
    callback: callable,
    char_limit: int = 0,
) -> None:
    """Ask a user for a string
    Pass a unique value for `reference` so that the response is routed correctly.
    `callback` will be called with the following positional args:
        1. reference
        2. user's response
    """

    promt_callbacks[reference] = callback

    user_socket.send(
        json.dumps.encode(
            {
                "promptString": True,
                "promptText": prompt,
                "promptRef": reference,
                "charLimit": char_limit,
            }
        )
    )


def toast(message: str, web_socket: WebSocket, skip_chat: bool = False, on_click=None):
    """ Tell a client to display a toast """
    web_socket.send(
        json.dumps(
            {
                "toast": True,
                "message": message,
                "skipChat": skip_chat,
                "onClick": on_click,
            }
        )
    )


def play_sound(sound: str, web_socket):
    """Tell the client to play a sound by name."""
    web_socket.send(json.dumps({"playSound": True, "sound": sound}))


def prompt_for_string(
    prompt: str, web_socket, reference: str, callback, char_limit: int = 0
):
    """
    Ask a user for a string.

    Pass a unique value for 'reference' so that the response is routed correctly.

    When the user submits a string, 'callback' will be called with the following arguments:
    1. reference: str
    2. user's response: str
    """
    global PROMPT_CALLBACKS

    if not PROMPT_CALLBACKS:
        PROMPT_CALLBACKS = dict()

    PROMPT_CALLBACKS[reference] = callback

    web_socket.send(
        json.dumps(
            {
                "promptString": True,
                "promptText": prompt,
                "promptRef": reference,
                "charLimit": char_limit,
            }
        )
    )


def wait_for(
    predicate: Union[bool, Callable[[], bool]],
    timeout: float = None,
    period: float = 0.25,
) -> None:
    start_time = time.time()
    end_time = None
    if timeout:
        end_time = start_time + timeout
    while True:
        if isinstance(predicate, Callable):
            predicate = predicate()
        if predicate:
            return
        if end_time and end_time > time.time():
            raise ValueError("Condition never became true within timeout.")
        time.sleep(period)


def clamp(value, max_value, min_value):
    """ Return [value] adjusted to exist between [min_value] and [max_value] """

    return max(min(value, max_value), min_value)
