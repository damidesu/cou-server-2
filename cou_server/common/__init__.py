import datetime
import functools
import logging
import os

from pathlib import Path

# Logging
trace_level_num = logging.DEBUG - 5


def log_for_level(self, message, *args, **kwargs):
    if self.isEnabledFor(trace_level_num):
        self._log(trace_level_num, message, args, **kwargs)


def log_to_root(message, *args, **kwargs):
    logging.log(trace_level_num, message, *args, **kwargs)


logging.addLevelName(trace_level_num, "TRACE")
logging.TRACE = trace_level_num
logging.getLoggerClass().trace = log_for_level
logging.trace = log_to_root

console_logging = logging.StreamHandler()
log_level = os.getenv("LEVEL", "DEBUG").upper()
console_logging.setLevel(getattr(logging, log_level))
console_logging.setFormatter(
    logging.Formatter(
        fmt="[%(levelname)s] [%(asctime)s] [%(name)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
)


def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(console_logging.level)
    logger.addHandler(console_logging)
    return logger


# Caching

CACHE_DIR = Path("_cache")

if not CACHE_DIR.is_dir():
    CACHE_DIR.mkdir()

# General global objects

TZ_UTC = datetime.timezone(datetime.timedelta(hours=0))


def timed(func):
    logger = get_logger(__name__)

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = datetime.datetime.now()
        result = func(*args, **kwargs)
        difference = datetime.datetime.now() - start_time
        total = difference.microseconds / 1000 + difference.seconds * 1000
        logger.trace(f"{func.__name__} took {total}ms")
        return result

    return wrapper
