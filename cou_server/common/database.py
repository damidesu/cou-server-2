from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from cou_server.api_keys import DATABASE_URI
from cou_server.common import get_logger

logger = get_logger(__name__)
local_engine = create_engine("sqlite:///cou_server.db")
remote_engine = create_engine(DATABASE_URI)
LocalSession = sessionmaker(bind=local_engine)
RemoteSession = sessionmaker(bind=remote_engine)
Base = declarative_base()


def db_create():
    """ Create or upgrade the database """

    # make sure all models are loaded first
    from cou_server import models

    Base.metadata.create_all(local_engine)
    logger.debug("local database ready")


@contextmanager
def db_session_context(local_db=False):
    """ Returns a session to the db which will be auto committed when the
        context exits.
    """

    if local_db:
        db_session = LocalSession()
    else:
        db_session = RemoteSession()
    try:
        yield db_session
        db_session.commit()
    except:
        db_session.rollback()
        raise
