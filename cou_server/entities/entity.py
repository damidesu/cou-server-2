from datetime import datetime, timedelta
import time
from typing import Union

import gevent

from cou_server.common import get_logger
from cou_server.endpoints.mapdata import tsid_l, streets
from cou_server.endpoints.time import utc_time
from cou_server.entities.action import Action
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.metabolics.metabolics_change import MetabolicsChange

logger = get_logger(__name__)


def create_id(x: float, y: float, entity_type: str, tsid: str):
    data_hash = hash(f"{entity_type}{x}{y}{tsid_l(tsid)}")
    return entity_type[0:1] + str(data_hash)


class IPersistable:
    def persist(self) -> None:  # TODO: StreetEntity
        """Called when the Street that the Entity is on is persisted to the database."""
        raise NotImplementedError()

    def restore_state(self, map_data: dict):
        """This will be called when the Entity is loaded from the db."""
        raise NotImplementedError()

    def get_persist_metadata(self) -> dict:
        """Called to get a map of all data that should be saved to the db."""
        raise NotImplementedError()


class IActionable:
    def customize_actions(self, email: str) -> list:
        """
        Called when sending the NPC's state to the client.

        In order to display accurate energy costs etc., we need to take the
        player's skills into account.
        """
        raise NotImplementedError()


class Entity(IPersistable, IActionable, MetabolicsChange):
    actions: list
    action_time: int = 2500
    x: float
    y: float
    z: float
    rotation: float
    street_name: str
    type: str
    id: str
    bubble_text: str = None
    say_timeout: datetime = None
    responses: dict
    states: dict
    current_state: SpriteSheet
    respawn: Union[datetime, None]
    gains: dict = {"energy": 0, "mood": 0, "img": 0, "currants": 0}

    def __init__(self):
        super().__init__()
        raise NotImplementedError()

    @property
    def entity_type(self):
        return type(self).__name__

    def find_action(self, action_name: str) -> Action or None:
        for action in self.actions:
            if action.action_name == action_name:
                return action

        return None

    def set_action_enabled(self, action_name: str, enabled: bool):
        try:
            for action in self.actions:
                if action.action_name == action_name:
                    action.enabled = enabled
                    return
        except Exception as e:
            logger.error(f"Error enabling/disabling action {action_name}: {e}")

    def get_persist_metadata(self) -> dict:
        return dict()

    def persist(self) -> None:  # TODO: StreetEntity
        street_data = streets()[self.street_name]
        if not street_data:
            logger.warning(
                f"Cannot persist {self.entity_type} entity {self.id} because street_data was null on {self.street_name}"
            )
            return None

        tsid = street_data["tsid"]
        if not tsid:
            logger.warning(
                f"Cannot persist {self.entity_type} entity {self.id} because tsid was null on {self.street_name}"
            )
            return None

        # TODO: return StreetEntity (https://gitlab.com/ChildrenOfUr/coUserver/blob/master/lib/entities/entity.dart#L200)

    def customize_actions(self, email: str) -> list:
        return self.actions

    def say(self, message: str = None):
        message = message.strip() if message else str()

        now = utc_time()
        if not self.say_timeout or self.say_timeout > now:
            self.bubble_text = message
            ttl = len(message) * 30 + 3000  # 3 second minimum plus 0.3 sec per char
            ttl = min(ttl, 10000)  # Cap at 10 sec

            duration = timedelta(milliseconds=ttl)
            self.say_timeout = now + duration

            def reset_say():
                time.sleep(duration.seconds)
                self.bubble_text = None
                self.reset_gains()

            gevent.spawn(reset_say)

    def set_state(
        self,
        state: str,
        repeat: int = 1,
        repeat_for: timedelta = None,
        then_state: str = None,
    ):
        if state not in self.states:
            raise ValueError(
                f"You made a typo: {state} does not exist in the states array for {self.entity_type}"
            )

        if then_state and then_state not in self.states:
            raise ValueError(
                f"You made a typo: {then_state} does not exist in the states array for {self.entity_type}"
            )

        # Set the state and the respawn time that it needs
        self.current_state = self.states[state]

        # If we want the animation to play more than once before respawn,
        # then multiply the length by the repeat.
        length = int(self.current_state.num_frames / 30 * 1000) * repeat

        if repeat_for:
            length = repeat_for.microseconds // 1000

        if then_state:
            # TODO: new Timer(new Duration(milliseconds: length), () => setState(thenState));
            length += int(self.states[then_state].num_frames / 30 * 1000)

        self.respawn = utc_time() + timedelta(milliseconds=length)

    def has_requirements(
        self,
        action_name: str,
        email: str,
        include_broken: bool = False,
        test_energy: bool = False,
    ) -> bool:
        action = self.find_action(action_name)

        # Check that the player has the necessary energy
        if test_energy:
            metabolics = None  # TODO: get_metabolics(email = email)
            if metabolics.energy < action.energy_requirements.energy_amount:
                return False

        # Check the players skill level(s) against the required skill level(s)
        for skill, level in action.skill_requirements:
            player_level = 0  # TODO: get_level(skill, email)
            if player_level < level:
                return False

        # Check that the player has the necessary item(s)

        has_at_least_one = len(action.item_requirements.any) == 0
        for item_type in action.item_requirements.any:
            if not has_at_least_one:
                if include_broken:
                    has_at_least_one = (
                        False
                    )  # TODO: Inventory.has_item(email, item_type, 1)
                else:
                    has_at_least_one = (
                        False
                    )  # TODO: Inventory.has_unbroken_item(email, item_type, 1, notify_if_broken = True)

        has_all = False
        for item_type, qty in action.item_requirements.all:
            if include_broken:
                has_all = False  # TODO: Inventory.has_item(email, item_type, qty)
            else:
                has_all = (
                    False
                )  # TODO: Inventory.has_unbroken_item(email, item_type, qty, notify_if_broken = True)

        return has_all

    def get_map(self):
        return {"bubbleText": self.bubble_text, "gains": self.gains}
