from cou_server.entities.spritesheet import SpriteSheet
from .door import Door

_ENTRANCES = {
    "Chego Chase": "LDOCV86VHCD245F",  # Andra Bureaucratic Hall
    "Baeli Bray": "LIF2E3LVGK82J7Q",  # Muufo Bureaucratic Hall
    "Gregarious Grange": "LIF101O8CDQ1AMU",  # Bureaucratic Hall
}

_EXITS = {
    "Andra Bureaucratic Hall": "LA5I10NJDL52TKD",  # Chego Chase
    "Muufo Bureaucratic Hall": "LA91JUQT2G82GUL",  # Baeli Bray
    "Bureaucratic Hall": "LLI32G3NUTD100I",  # Gregarious Grange
}

INTERIOR = SpriteSheet(
    state_name="door_bh_ext",
    url="https://childrenofur.com/assets/entityImages/door_asset_bureaucratic_hall_ext.svg",
    sheet_width=116,
    sheet_height=122,
    frame_width=116,
    frame_height=122,
    num_frames=1,
    loops=True,
)

EXTERIOR = SpriteSheet(
    state_name="door_bh_int",
    url="https://childrenofur.com/assets/entityImages/door_asset_bureaucratic_hall_int.svg",
    sheet_width=132,
    sheet_height=312,
    frame_width=132,
    frame_height=312,
    num_frames=1,
    loops=True,
)


class BureaucraticHallDoor(Door):
    def __init__(self, entity_id: str, street_name: str, x, y):
        if street_name in _ENTRANCES:
            # Door is outside a Bureaucratic Hall going in
            self.to_location = _ENTRANCES[street_name]
            self.outside = True
            self.current_state = EXTERIOR
        elif street_name in _EXITS:
            # Door is inside a Bureaucratic Hall going out
            self.to_location = _EXITS[street_name]
            self.outside = False
            self.current_state = INTERIOR
        else:
            raise (
                "Cannot instantiate a Bureaucratic Hall Door because an invalid "
                "bureaucratic hall location was specified: " + street_name
            )

        self.type = "Bureaucratic Hall"
        super().__init__(entity_id, street_name, x, y)
