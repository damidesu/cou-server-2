from json import dumps

from cou_server.entities.entity import Entity
from cou_server.entities.action import Action


ENTER_ACTION = Action("enter", "walking in")
EXIT_ACTION = Action("exit", "walking out")

class Door(Entity):
    to_location: str
    street_name: str
    outside: bool

    def __init__(self, entity_id, street_name, x, y):
        super().__init__()
        self.id = entity_id
        self.street_name = street_name
        self.x = x
        self.y = y
        self.type = "Door"

        if self.outside:
            self.actions = [ENTER_ACTION]
        else:
            self.actions = [EXIT_ACTION]

    def persist(self):
        # For now, nothing about doors needs to be persisted to the db
        return None

    def restore_state(self, map_data: dict):
        pass

    def enter(self, user_socket, email):
        self.use_door(user_socket, email)

    def exit(self, user_socket, email):
        self.use_door(user_socket, email)

    def use_door(self, user_socket, email):
        user_socket.send(dumps({
            "gotoStreet": "true",
            "tsid": self.to_location
        }))

    def __dict__(self):
        return super().__dict__.update({
            "id": self.id,
            "url": self.current_state.url,
            "type": self.type,
            "numRows": self.current_state.num_rows,
            "numColumns": self.current_state.num_cols,
            "numFrames": self.current_state.num_frames,
            "state": 0,
            "actions": [action.__dict__ for action in self.actions],
            "x": self.x,
            "y": self.y,
        })
