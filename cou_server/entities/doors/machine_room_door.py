from cou_server.entities.spritesheet import SpriteSheet
from .door import Door

_ENTRANCES = {
    "Canary Send": "LA9MU59GB792T80",  # Besara Community Machine Room
    "Middle Valley Clearing": "LIF16EM56A12FSB",  # Forest Community Machine Room
    "Otterlane": "LCR101Q98A12EHH",  # Meadow Community Machine Room
}

_EXITS = {
    "Besara Community Machine Room": "LA9G6MA4P6923PH",  # Canary Send
    "Forest Community Machine Room": "LLI23FELDHD1O3C",  # Middle Valley Clearing
    "Meadow Community Machine Room": "LCR10K3BPJK1U6F",  # Otterlane
}

INTERIOR = SpriteSheet(
    state_name="door_mr_int",
    url="https://childrenofur.com/assets/entityImages/door_asset_community_machine_room_ext.svg",
    sheet_width=57,
    sheet_height=244,
    frame_width=57,
    frame_height=244,
    num_frames=1,
    loops=True,
)

EXTERIOR = SpriteSheet(
    state_name="door_mr_ext",
    url="https://childrenofur.com/assets/entityImages/machine_room_door_int.png",
    sheet_width=57,
    sheet_height=244,
    frame_width=57,
    frame_height=244,
    num_frames=1,
    loops=True,
)


class MachineRoomDoor(Door):
    def __init__(self, entity_id: str, street_name: str, x, y):
        if street_name in _ENTRANCES:
            # Door is outside a Machine Room going in
            self.to_location = _ENTRANCES[street_name]
            self.outside = True
            self.current_state = EXTERIOR
        elif street_name in _EXITS:
            # Door is inside a Machine Room going out
            self.to_location = _EXITS[street_name]
            self.outside = False
            self.current_state = INTERIOR
        else:
            raise (
                "Cannot instantiate a Machine Room Door because an invalid "
                "machine room location was specified: " + street_name
            )

        self.type = "Machine Room"
        super().__init__(entity_id, street_name, x, y)
