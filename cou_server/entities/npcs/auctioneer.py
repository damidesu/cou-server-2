from json import dumps

from cou_server.entities.action import Action
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

STATES = {
    "idle": SpriteSheet(
        state_name="idle",
        url="https://childrenofur.com/assets/entityImages/npc_rare_item_vendor__x1_idle_png_1354840068.png",
        sheet_width=935,
        sheet_height=2002,
        frame_width=187,
        frame_height=91,
        num_frames=109,
        loops=True,
        loop_delay=5000,
    ),
    "talk": SpriteSheet(
        state_name="talk",
        url="https://childrenofur.com/assets/entityImages/npc_rare_item_vendor__x1_talk_png_1354840071.png",
        sheet_width=935,
        sheet_height=1365,
        frame_width=187,
        frame_height=91,
        num_frames=72,
        loops=False,
    ),
    "walk": SpriteSheet(
        state_name="walk",
        url="https://childrenofur.com/assets/entityImages/npc_rare_item_vendor__x1_walk_png_1354840072.png",
        sheet_width=748,
        sheet_height=364,
        frame_width=187,
        frame_height=91,
        num_frames=16,
        loops=True,
    ),
}


class Auctioneer(NPC):
    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.action_time = 0
        self.actions += [Action("Talk to")]
        self.type = "Auctioneer"
        self.speed = 0
        self.states = STATES
        self.set_state("idle")

    def update(self, simulate_tick: bool = False) -> None:
        super().update(simulate_tick)

    def talk_to(self, user_socket=None, email: str = None):
        return user_socket.send(dumps({"vendorName": self.type, "id": self.entity_id}))
