from json import dumps

from cou_server.common.util import toast
from cou_server.entities.action import Action
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

VISIT = Action("visit a street", action_word="visiting")

SPRITE = SpriteSheet(
    state_name="up",
    url="https://childrenofur.com/assets/entityImages/visiting_stone__x1_1_png_1354840181.png",
    sheet_width=101,
    sheet_height=155,
    frame_width=101,
    frame_height=105,
    num_frames=1,
    loops=True,
)


class VisitingStone(NPC):
    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.type = "Visiting Stone"
        self.action_time = 0
        self.actions += VISIT
        self.current_state = SPRITE

        # Automatically face the correct direction
        # based on the assumed end of the street
        if self.x <= 500:
            self.facing_right = self.x <= 500

    def random_tsid(self, email) -> str:
        # TODO: randomUnvisitedTsid(email, inclHidden: false)
        pass

    def update(self, simulate_tick: bool = False) -> None:
        # The status is set in stone
        super().update(simulate_tick)

    def visit_a_street(self, email: str = None, user_socket=None):
        tsid = self.random_tsid(email)

        if tsid == "ALL_VISITED":
            toast("You've visited every street in the game!", user_socket)
        elif tsid:
            user_socket.send(dumps({"gotoStreet": "true", "tsid": tsid}))
        else:
            toast("Something went wrong :(", user_socket)
