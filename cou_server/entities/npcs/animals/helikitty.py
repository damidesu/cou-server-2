from datetime import timedelta, datetime
from random import choice, randint

from cou_server.common.util import play_sound
from cou_server.entities.action import Action, EnergyRequirements
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

SKILL = "animal_kinship"

PET = Action("pet", action_word="petting")
PET.energy_requirements = EnergyRequirements(5)

RESPONSES = {
    "pet": [
        ":3"
    ]
}

STATES = {
    # Variation 1:
    # Newborn
    "1blink": SpriteSheet(
        "1blink",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1blink_png_1354840541.png",
        680,
        115,
        136,
        115,
        5,
        False,
    ),
    "1jumpAntic": SpriteSheet(
        "1jumpAntic",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1jumpAntic_png_1354840542.png",
        544,
        230,
        136,
        115,
        8,
        False,
    ),
    "1jump": SpriteSheet(
        "1jump",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1jump_png_1354840543.png",
        816,
        345,
        136,
        115,
        16,
        True,
    ),
    "1rollStart": SpriteSheet(
        "1rollStart",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1rollStart_png_1354840544.png",
        272,
        115,
        136,
        115,
        2,
        False,
    ),
    "1roll": SpriteSheet(
        "1roll",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1roll_png_1354840544.png",
        816,
        230,
        136,
        115,
        12,
        True,
    ),
    "1sleepStart": SpriteSheet(
        "1sleepStart",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1sleepStart_png_1354840546.png",
        952,
        460,
        136,
        115,
        26,
        False,
    ),
    "1sleep": SpriteSheet(
        "1sleep",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_1sleep_png_1354840547.pngg",
        952,
        1035,
        136,
        115,
        57,
        True,
    ),
    # Variation 2:
    # Kitten
    "2blink": SpriteSheet(
        "2blink",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_2blink_png_1354840549.png",
        680,
        115,
        136,
        115,
        20,
        True,
    ),
    "2fly": SpriteSheet(
        "2fly",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3fly_png_1354840558.png",
        952,
        345,
        136,
        115,
        20,
        True,
    ),
    "2hitBall": SpriteSheet(
        "2hitBall",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_2hitBall_png_1354840552.png",
        680,
        230,
        136,
        115,
        9,
        False,
    ),
    "2jumpAntic": SpriteSheet(
        "2jumpAntic",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_2jumpAntic_png_1354840549.png",
        544,
        230,
        136,
        115,
        8,
        False,
    ),
    "2jump": SpriteSheet(
        "2jump",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_2jump_png_1354840550.png",
        816,
        345,
        136,
        115,
        16,
        True,
    ),
    "2sleepStart": SpriteSheet(
        "2sleepStart",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_2sleepStart_png_1354840553.png",
        952,
        460,
        136,
        115,
        26,
        False,
    ),
    "2sleep": SpriteSheet(
        "2sleep",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_2sleep_png_1354840554.png",
        952,
        1035,
        136,
        115,
        26,
        True,
    ),
    # Variation 3:
    # Adult
    "3appear": SpriteSheet(
        "3appear",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3appear_png_1354840565.png",
        952,
        805,
        136,
        115,
        47,
        False,
    ),
    "3blink": SpriteSheet(
        "3blink",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3blink_png_1354840555.png",
        680,
        115,
        136,
        115,
        5,
        False,
    ),
    "3chew": SpriteSheet(
        "3chew",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3chew_png_1354840562.png",
        680,
        345,
        136,
        115,
        15,
        True,
    ),
    "3disappear": SpriteSheet(
        "3disappear",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3disappear_png_1354840566.png",
        952,
        460,
        136,
        115,
        27,
        False,
    ),
    "3fly": SpriteSheet(
        "3fly",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3fly_png_1354840558.png",
        952,
        345,
        136,
        115,
        20,
        True,
    ),
    "3happy": SpriteSheet(
        "3happy",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3happy_png_1354840564.png",
        952,
        690,
        136,
        115,
        38,
        False,
    ),
    "3hitBall": SpriteSheet(
        "3hitBall",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3hitBall_png_1354840558.png",
        680,
        230,
        136,
        115,
        9,
        False,
    ),
    "3jumpAntic": SpriteSheet(
        "3jumpAntic",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3jumpAntic_png_1354840556.png",
        544,
        230,
        136,
        115,
        8,
        False,
    ),
    "3jump": SpriteSheet(
        "3jump",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3jump_png_1354840557.png",
        816,
        345,
        136,
        115,
        16,
        True,
    ),
    "3sad": SpriteSheet(
        "3sad",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3sad_png_1354840563.png",
        816,
        345,
        136,
        115,
        17,
        False,
    ),
    "3sleepStart": SpriteSheet(
        "3sleepStart",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3sleepStart_png_1354840559.png",
        952,
        460,
        136,
        115,
        26,
        False,
    ),
    "3sleep": SpriteSheet(
        "3sleep",
        "https://childrenofur.com/assets/entityImages/npc_kitty_chicken__x1_3sleep_png_1354840561.png",
        952,
        1035,
        136,
        115,
        57,
        True,
    ),
}


class HeliKitty(NPC):
    age: int = 3  # TODO: grow up

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.type = "Heli Kitty"
        self.actions += [PET]
        self.speed = 75  # px/s
        self.can_rename = True
        self.age = 3
        self.states = STATES
        self.set_state("fly")

    def set_state(
        self,
        state: str,
        repeat: int = 1,
        repeat_for: timedelta = None,
        then_state: str = None,
    ):
        sheet_name = f"{self.age}{state}"
        super().set_state(sheet_name, repeat, repeat_for, then_state)

    def pet(self, user_socket = None, email: str = None):
        success = True  # TODO: super.trySetMetabolics
                        #  (email, energy:-5, mood:20, imgMin:10, imgRange:4)
        if not success:
            return False

        self.set_state("hitBall")
        # TODO: StatManager.add(email, Stat.heli_kitties_petted)
        # TODO: SkillManager.learn(SKILL, email)
        play_sound("purr", user_socket)
        self.say(choice(RESPONSES["pet"]))
        return True

    def update(self, simulate_tick: bool = False):
        super().update(simulate_tick)

        if "fly" in self.current_state.state_name:
            self.move_xy(y_action=self.y_action, ledge_action=self.ledge_action)

        # If respawn is in the past, it is time to choose a new animation
        if self.respawn and self.respawn < datetime.utcnow():
            self.set_state("fly")
            self.respawn = None

            # 50% chance to change direction
            if randint(0, 1) == 1:
                self.turn_around()

    def y_action(self):
        pass

    def ledge_action(self):
        pass
