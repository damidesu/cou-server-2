from datetime import datetime, timedelta
from random import randint, choice
from json import loads, dumps

from cou_server.entities.action import Action, ItemRequirements, EnergyRequirements
import cou_server.entities.items.item as items
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

SKILL = "animal_kinship"

KFC_DURATION = timedelta(seconds=3)

EGG_ANIMALS = {
    "butterfly_egg": "caterpillar",
    "chicken_egg": "chick",
    "piggy_egg": "piglet",
}

INCUBATE_ITEM_REQ = ItemRequirements()
INCUBATE_ITEM_REQ.any = list(EGG_ANIMALS.keys())
INCUBATE_ITEM_REQ.error = "You'll need an animal egg for the chicken to incubate."

SQUEEZE = Action("squeeze", action_word="squeezing")
SQUEEZE.energy_requirements = EnergyRequirements(5)

INCUBATE = Action("incubate", action_word="incubating")

RESPONSES = {
    "squeeze": [
        "Yeeeeeeeeeeeeeks!",
        "Oh my beak and giblets, you scared me!",
        "You made me drop my GRAIN, squeezefiend!",
        "Squeeze. Grain. Grain. Squeeze. It's a chicken's life.",
        "BUK! Take the grain! Take it!",
        "Again with the squeezing?!?",
        "So take it - I didn't want that grain anyway.",
        "Squeezed again? Oy. Such imagination you show.",
        "What IS this, the world chicken wrestling featheration?",
        "One day, chickens squeeze YOU.",
        "Another squeeze? Really?!?",
        "HELP! Chickenmuggings!",
        "Fine. Take it. And enjoy, grain-finagler.",
        "Buk-buk-buk. That what you want to hear?",
        "Squeeze squeeze squeeze squeeze squeeze. Buk.",
        "Not so hard, you'll tangle my intestinal noodles.",
        "Yes, because chickens don't need personal space too? Pah.",
        "Consider my feathers ruffled. Buk.",
        "Chicken-ruffler! Alarm! Alarm!",
        "Always with the squeezing!",
        "Oh look. It's Chicken Wrestler. Again.",
        "Rummage all you like, I've only got grain.",
        "Grain! Grain if you'll stop!",
        "Buk! Off! Get off! Buk buk.",
        "Do YOU like to be squeezed by random strangers? Hmn?",
        "Oooh, CHASE ME!",
        "Chicken-botherer begone! Take the grain already!",
        "Consider me squeezed. Squoozed? Squzz?",
        "Psssst! I don't mind really.",
        "One day: revenge. Until that day: grain.",
        "Buy grain on auction, maybe? No? Just squeezing? OK!",
        "Oh go on then: one more squeeze. Hic!",
    ],
    "squeezeExtra": [
        "Take it! Take it ALL!",
        "Well done you.",
        "All the grain. Happy?",
        "If give you this, will you hold off on the squeezing?",
        "Take it all! Take it and go!",
        "So now you want extra? Buk.",
        "Like you deserve this, squeezy scourge of chicken.",
        "Happy now?",
        "Congrainulations, you superscored.",
        "STOP SQUEEZING ME!",
        "THERE! Anything else? Arm? Leg? Gravy? Peh.",
        "Happy chickenannoying day.",
        "My little hot pockets are emptied of grain.",
        "Are we done here?",
        "Grain. Because that's ALL I'm good for. *sigh*.",
        "Your reward for all your chickensquooging! Buk.",
        "Oh! So hard a squeeze! Supergrain!",
        "Yes, yes. A squeeze, some grain, same old…",
        "You either really like grain, or get some kick out of this.",
        "Yes, yes: congrainulations, squeezefiend.",
        "You deserve this, squeezefiend. Your arms must be tired.",
        "You've emptied my chicken pockets! Happy now?",
        "Truth? I quite like squeezing. Don't stop.",
    ],
    "squeezeFail": [
        "Squaaaaaaahahaha! Too fast for you!",
        "Buk! No squeeze! I greased my feathers!",
    ],
    "incubateStart": [
        "Now the chicken is superior, eh? Wait here ONE MINUTE and I'll give it back.",
        "No squeezing while I sit? Ok deal. But, you can't leave for a whole minute. Stay, if you want your animal.",
        "Well, comfortable it isn't... but ok. Deal is that you have to wait a full minute.",
        "I'm egg-static to be of service. That was sarcasm. But whatever ... if you stick around for a minute I'll get 'er done.",
        "At least you appreciate my warm underfeatheredside.  Love it! Also, stick around for sixty seconds or lose it!",
    ],
    "incubateEnd": [
        "Ping!",
        "Done! What were you expecting, 3 1\/2 minutes?",
        "Buk! It bit my butt! You owe me a beer.",
        "Here. Another new life. A miracle. Thank me later.",
        "Ta DA!",
    ],
    "incubateFail": ["Can't you see I'm busy right now?"],
}


STATES = {
    "fall": SpriteSheet(
        "fall",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_fall_png_1354830392.png",
        740,
        550,
        148,
        110,
        25,
        True,
    ),
    "flying_back": SpriteSheet(
        "flying_back",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_flying_back_png_1354830391.png",
        888,
        330,
        148,
        110,
        17,
        True,
    ),
    "flying_no_feathers": SpriteSheet(
        "flying_no_feathers",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_flying_no_feathers_png_1354830388.png",
        888,
        770,
        148,
        110,
        42,
        True,
    ),
    "flying": SpriteSheet(
        "flying",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_flying_png_1354830387.png",
        888,
        770,
        148,
        110,
        42,
        True,
    ),
    "idle1": SpriteSheet(
        "idle1",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_idle1_png_1354830404.png",
        888,
        1320,
        148,
        110,
        67,
        False,
    ),
    "idle2": SpriteSheet(
        "idle2",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_idle2_png_1354830405.png",
        888,
        880,
        148,
        110,
        47,
        False,
    ),
    "idle3": SpriteSheet(
        "idle3",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_idle3_png_1354830407.png",
        888,
        1650,
        148,
        110,
        86,
        False,
    ),
    "incubate": SpriteSheet(
        "incubate",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_incubate_png_1354830403.png",
        888,
        3520,
        148,
        110,
        190,
        False,
    ),
    "land_on_ladder": SpriteSheet(
        "land_on_ladder",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_land_on_ladder_png_1354830390.png",
        888,
        550,
        148,
        110,
        26,
        False,
    ),
    "land": SpriteSheet(
        "land",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_land_png_1354830389.png",
        740,
        550,
        148,
        110,
        25,
        False,
    ),
    "pause": SpriteSheet(
        "pause",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_pause_png_1354830395.png",
        888,
        2420,
        148,
        110,
        131,
        False,
    ),
    "pecking_once": SpriteSheet(
        "pecking_once",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_pecking_once_png_1354830398.png",
        888,
        660,
        148,
        110,
        32,
        False,
    ),
    "pecking_twice": SpriteSheet(
        "pecking_twice",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_pecking_twice_png_1354830400.png",
        888,
        770,
        148,
        110,
        27,
        False,
    ),
    "rooked2": SpriteSheet(
        "rooked2",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_rooked2_png_1354830409.png",
        888,
        550,
        148,
        110,
        27,
        False,
    ),
    "sit": SpriteSheet(
        "sit",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_sit_png_1354830401.png",
        740,
        440,
        148,
        110,
        20,
        False,
    ),
    "verb": SpriteSheet(
        "verb",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_verb_png_1354830397.png",
        888,
        1100,
        148,
        110,
        55,
        False,
    ),
    "walk": SpriteSheet(
        "walk",
        "https://childrenofur.com/assets/entityImages/npc_chicken__x1_walk_png_1354830385.png",
        888,
        440,
        148,
        110,
        24,
        True,
    ),
}


class Chicken(NPC):
    incubating: bool = False
    squeeze_record: dict = dict()
    last_reset: datetime = datetime.utcnow()
    kfc_end: datetime = None

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.actions += [SQUEEZE, INCUBATE]
        self.type = "Chicken"
        self.speed = 75  # px/s
        self.can_rename = True
        self.states = STATES
        self.set_state("walk")

        # TODO:
        #  Clock clock = new Clock();
        #  clock.onNewDay.listen((_) => _resetLists());

        # TODO:
        #  messageBus.subscribe(ChatEvent, this, whereFunc: (ChatEvent event) {
        # 			return (event.streetName == this.streetName) && (event.message.toLowerCase().contains('kfc'));
        # 		});

    def reset_lists(self):
        self.squeeze_record.clear()
        self.last_reset = datetime.utcnow()

    def restore_state(self, map_data: dict):
        super().restore_state(map_data)

        if "squeezeList" in map_data:
            self.squeeze_record = loads(map_data["squeezeList"])

        if "lastReset" in map_data:
            self.last_reset = datetime.utcfromtimestamp(
                int(map_data["lastReset"]) // 1000
            )

            # TODO:
            #  Clock lastResetClock = new Clock.stoppedAtDate(lastReset);
            # 			Clock currentClock = new Clock.stoppedAtDate(new DateTime.now());
            # 			if (lastResetClock.dayInt < currentClock.dayInt ||
            # 				lastResetClock.hourInt < 6 && currentClock.hourInt >= 6) {
            # 				_resetLists();
            # 			}
            # 		}

    def get_persist_metadata(self):
        return (
            super()
            .get_persist_metadata()
            .update(
                {
                    "squeezeList": self.squeeze_record,
                    "lastReset": self.last_reset.timestamp() * 1000,
                }
            )
        )

    def squeeze(self, user_socket=None, email: str = None):
        level = 0  # SkillManager.getLevel(SKILL, email)
        energy = -1
        mood = 2 * (level + 1)
        img_min = 3 * (level + 1)
        img_range = 2 * (level + 1) // 3
        count = 1
        odds = 20

        if level == 0:
            # 1 in 3 chance to fail
            if randint(1, 3) == 1:
                self.say(choice(RESPONSES["squeezeFail"]))
                return False
        elif level == 7:
            count = 4
            odds = 5
        elif level > 4:
            count = 3
            odds = 10
        elif level > 1:
            count = 2
            odds = 15

        # TODO: trySetMetabolics
        #  (email, energy: energy, mood: mood, imgMin:imgMin, imgRange: imgRange)
        success = True

        if not success:
            return False

        # TODO: SkillManager.learn(SKILL, email)

        if randint(odds) == 1:
            # 1 / odds chance of bonus
            count += 3
            self.say(choice(RESPONSES["squeezeExtra"]))
        else:
            self.say(choice(RESPONSES["squeeze"]))

        # TODO: InventoryV2.addItemToUser(email, items['grain'].getMap(), count, id)

        # TODO: StatManager.add(email, Stat.chickens_squeezed)

        # Check achievement
        total_squeezed = 0  # TODO: StatManager.get(email, Stat.chickens_squeezed)
        if total_squeezed >= 503:
            # TODO: Achievement.find("the_hugginator").awardTo(email)
            pass
        elif total_squeezed >= 137:
            # TODO: Achievement.find("hen_hugger_supremalicious").awardTo(email)
            pass
        elif total_squeezed >= 41:
            # TODO: Achievement.find("hen_hugger_deluxe").awardTo(email)
            pass
        elif total_squeezed >= 11:
            # TODO: Achievement.find("hen_hugger").awardTo(email)
            pass

        return True

    def incubate(self, user_socket, email: str = None):
        if not self.incubating:
            # Not busy
            user_socket.send(
                dumps(
                    {
                        "action": "incubate_2",
                        "id": self.id,
                        "openWindow": "itemChooser",
                        "filter": "itemType=" + "|".join(EGG_ANIMALS.keys()),
                        "windowTitle": "Incubate what?",
                    }
                )
            )
            return True
        else:
            # Busy
            self.say(choice(RESPONSES["incubateFail"]))
            return False

    def incubate_2(
        self,
        user_socket=None,
        email: str = None,
        item_type: str = None,
        count: int = None,
        slot: int = None,
        sub_slot: int = None,
    ):
        # Take egg from player
        if not True:  # TODO: InventoryV2.takeItemFromUser(email, slot, subSlot, 1)
            # Taking item failed
            return False

        self.incubating = True

        # Say a random starting message
        self.say(choice(RESPONSES["incubateStart"]))

        # Sit down on the egg
        self.set_state("incubate")

        # Wait 1 minute
        # TODO: await new Future.delayed(new Duration(minutes: 1))

        # Sit down without the egg
        self.set_state("sit")

        # Say a random ending message
        self.say(choice(RESPONSES["incubateEnd"]))

        # Give the baby animal to the player
        animal = items.Item.clone(EGG_ANIMALS[item_type])
        inv_success = False  # TODO: InventoryV2.addItemToUser
        #  (email, animal.getMap(), 1)) == 1

        self.incubating = False
        return inv_success

    def update(self, simulate_tick: bool = False):
        super().update(simulate_tick)

        # Update x and y
        if self.current_state.state_name in {"walk", "flying"}:
            self.move_xy()

        if self.kfc_end:
            if self.kfc_end < datetime.utcnow():
                # Done flipping out
                self.kfc_end = None
                self.move_xy()
            else:
                # Not done flipping out
                self.set_state("flying")
                self.y -= 5
        elif self.respawn and datetime.utcnow() > self.respawn:
            # If respawn is in the past, it is time to choose a new animation
            # 1 in 8 chance to change direction
            if randint(1, 8) == 1:
                self.turn_around()

            if not self.incubating:
                num = randint(1, 20)

                if 1 <= num <= 3:
                    self.set_state(f"idle{num}")
                elif num == 4:
                    self.set_state("pause")
                elif num == 5:
                    self.set_state("pecking_once")
                elif num == 6:
                    self.set_state("pecking_twice")
                elif num == 7:
                    self.set_state("flying")
                else:
                    self.set_state("walk")

    def customize_actions(self, email: str):
        ak_level = 0  # TODO: await SkillManager.getLevel(SKILL, email)
        actions = list()

        for action in super().customize_actions(email):
            personal_action = Action(action)
            if action.action_name == "squeeze":
                # Chickens can only be squeezed once per day unless AK > 0
                # then twice per day unless AK > 4
                # then only thrice
                times = (
                    self.squeeze_record[email] if email in self.squeeze_record else 0
                )

                if ak_level > 6:
                    personal_action.time_required //= 3

                if ak_level > 4:
                    personal_action.time_required //= 2

                    if times >= 3:
                        personal_action.enabled = False
                        personal_action.error = (
                            "You can only squeeze this chicken thrice per day."
                        )
                elif ak_level > 0:
                    if times >= 2:
                        personal_action.enabled = False
                        personal_action.error = (
                            "You can only squeeze this chicken twice per day"
                        )
                else:
                    if times >= 1:
                        personal_action.enabled = False
                        personal_action.error = (
                            "You can only squeeze this chicken once per day"
                        )

                # It's harder to do when you're low level
                if ak_level == 0:
                    personal_action.energy_requirements.energy_amount = 3

                # The cost of squeezing goes up, but so do the rewards so that's fine
                if ak_level > 2:
                    personal_action.energy_requirements.energy_amount = 2
            elif action.action_name == "incubate":
                # You can only incubate eggs if you have at least AK 3
                if ak_level < 3:
                    personal_action.enabled = False
                    personal_action.error = (
                        "You need at least level 3 of Animal Kinship to incubate an egg"
                    )

            actions += [personal_action]

        return actions

    def handle_event(self, event):
        if not self.kfc_end:
            self.kfc_end = datetime.utcnow()

        self.kfc_end += KFC_DURATION
