from datetime import datetime, timedelta
from json import dumps
from random import randint

from cou_server.common.util import toast
import cou_server.entities.items.item as items
from cou_server.entities.action import Action, ItemRequirements
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

HEADPHONES = items.ITEMS["crabpod_headphones"].__dict__
CRABATO = items.ITEMS["crabato_juice"].__dict__
JUKEBOX = items.ITEMS["musicblock_bag"].__dict__

ERR_NO_MUSIC = "You're musicblock-broke, yo."
ERR_BUSY = "Go away, I'm busy right now!"
WARN_HEADPHONES = "You stole my headphones! No juice for you!"

MUSICBLOCK_TYPES = [
    "musicblock_bb_1",
    "musicblock_bb_2",
    "musicblock_bb_3",
    "musicblock_bb_4",
    "musicblock_bb_5",
    "musicblock_db_1",
    "musicblock_db_2",
    "musicblock_db_3",
    "musicblock_db_4",
    "musicblock_db_5",
    "musicblock_dg_1",
    "musicblock_dg2",
    "musicblock_dg3",
    "musicblock_dg4",
    "musicblock_dg5",
    "musicblock_dr_1",
    "musicblock_dr_2",
    "musicblock_dr_3",
    "musicblock_dr_4",
    "musicblock_dr_5",
    "musicblock_xs_1",
    "musicblock_xs_2",
    "musicblock_xs_3",
    "musicblock_xs_4",
    "musicblock_xs_5",
]

MUSICBLOCK_RARES = ["musicblock_gng", "musicblock_stoot", "musicblock_trumpets"]

ALL_MUSICBLOCK_TYPES = MUSICBLOCK_TYPES + MUSICBLOCK_RARES

MUSICBLOCK_ITEM_REQS = ItemRequirements()
MUSICBLOCK_ITEM_REQS.any = ALL_MUSICBLOCK_TYPES
MUSICBLOCK_ITEM_REQS.error = ERR_NO_MUSIC

PLAY = Action("play for", action_word="crabbing")
PLAY.item_requirements = MUSICBLOCK_ITEM_REQS

BUY = Action("buy crabpack", action_word="buying")
BUY.description = "A 3,000-currant bag for 18 musicblocks"

STATES = {
    "dislike_off": SpriteSheet(
        "dislike_off",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_dislike_off_png_1354831193.png",
        786,
        516,
        131,
        129,
        22,
        True,
    ),
    "dislike_on": SpriteSheet(
        "dislike_on",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_dislike_on_png_1354831191.png",
        786,
        516,
        131,
        129,
        30,
        True,
    ),
    "idle0": SpriteSheet(
        "idle0",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_idle0_png_1354831199.png",
        786,
        645,
        131,
        129,
        30,
        True,
    ),
    "idle1": SpriteSheet(
        "idle1",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_idle1_png_1354831200.png",
        786,
        645,
        131,
        129,
        30,
        True,
    ),
    "idle2": SpriteSheet(
        "idle2",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_idle2_png_1354831201.png",
        786,
        645,
        131,
        129,
        30,
        True,
    ),
    "like_off": SpriteSheet(
        "like_off",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_like_off_png_1354831189.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
    "like_on": SpriteSheet(
        "like_on",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_like_on_png_1354831187.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
    "listen": SpriteSheet(
        "listen",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_listen_png_1354831185.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
    "talk": SpriteSheet(
        "talk",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_talk_png_1354831203.png",
        917,
        1161,
        131,
        129,
        58,
        True,
    ),
    "walk": SpriteSheet(
        "walk",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_walk_png_1354831183.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
}


def rand_song_length():
    """Find how long a song will play for (5-15 seconds)."""
    return timedelta(seconds=randint(5, 15))


def rand_react_length():
    """Find how long the crab dances (3-8 seconds)."""
    return timedelta(seconds=randint(3, 8))


class Crab(NPC):
    listen_history: list = list()
    busy_email: str = str()

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        # Choose a random type of crab (there are different styles)
        self.idle_type = randint(1, 3)

        self.type = "Crab"
        self.speed = 60  # px/s
        self.action_time = 0
        self.actions += [PLAY, BUY]
        self.states = STATES
        self.idle()

    def idle(self):
        self.set_state("idle" + self.idle_type)

    def update(self, simulate_tick: bool = False) -> None:
        if self.busy_email:
            return

        super().update(simulate_tick)

        walking = self.current_state.state_name == "walk"
        if walking:
            self.move_xy()

        if self.respawn and datetime.utcnow() > self.respawn:
            # 1 in 8 chance to change direction
            if randint(1, 8) == 1:
                self.turn_around()

            chance = randint(1, 5)
            if chance > 3 or (chance > 2 and walking):
                self.set_state("walk")
            else:
                self.idle()

    def play_for(self, user_socket=None, email: str = None):
        user_socket.send(
            dumps(
                {
                    "action": "playMusic",
                    "id": self.entity_id,
                    "openWindow": "itemChooser",
                    "filter": "itemType=" + "|".join(ALL_MUSICBLOCK_TYPES),
                    "windowTitle": "Play what for Crab?",
                }
            )
        )

    def buy_crabpack(self, user_socket=None, email: str = None):
        metabolics = (None,)  # TODO: get_metabolics(email)
        if metabolics.currants >= JUKEBOX["price"]:
            metabolics.currants -= JUKEBOX["price"]
            # TODO: set_metabolics(metabolics)
            # TODO: Inventory.add_item_to_usre(email, JUKEBOX, 1, entity_id)
        else:
            toast("You can't afford to do that", user_socket)

    def add_to_history(self, music):
        """
        Add a song to the history of the crab.
        If it is already in the list, it is moved to the end.
        """
        if music in self.listen_history:
            self.listen_history.remove(music)

        self.listen_history.append(music)

    def likes_song(self, music) -> bool:
        """
        "A crab likes a song if it is in the first half of the list
        (sorted oldest to newest).
        """
        return (
            music not in self.listen_history
            or self.listen_history.index(music) < len(self.listen_history) // 2
        )

    def until_respawn(self) -> timedelta:
        return self.respawn - datetime.utcnow()

    def play_music(self, user_socket, email, item_type, count, slot, sub_slot):
        if self.busy_email:
            # Only 1 player at a time
            self.say(ERR_BUSY)
            return

        assert user_socket
        assert email
        assert item_type and item_type in ALL_MUSICBLOCK_TYPES

        self.busy_email = email

        def _take_musicblock():
            # TODO: InventoryV2.takeItemFromUser(email, slot, subSlot, 1)
            pass

        def _give_musicblock():
            # TODO: InventoryV2.addItemToUser(email, items[itemType].getMap(), 1)
            pass

        def _give_headphones():
            # TODO: InventoryV2.addItemToUser(email, HEADPHONES, 1, id)
            pass

        def _take_headphones() -> int:
            # TODO: InventoryV2.takeAnyItemsFromUser(email, HEADPHONES["itemType"], 1)
            pass

        is_rare = item_type in MUSICBLOCK_RARES

        if not _take_musicblock():
            # Could not take musicblock from player
            self.say(ERR_NO_MUSIC)
            self.busy_email = str()
            return

        _give_headphones()
        self.set_state("listen")

        # TODO: wait rand_song_length() + self.until_respawn()

        # Reward player
        if self.likes_song(item_type):
            # Dance for a bit
            self.set_state("like_on")
            # TODO: wait rand_react_length() + until_respawn()

            self.set_state("like_off")
            _give_musicblock()

            if _take_headphones() < 1:
                # Collectible headphones were stolen
                self.say(WARN_HEADPHONES)
            else:
                # Headphones returned, award crabato juice
                # TODO: InventoryV2.addItemToUser(email, CRABATO, 1, id)
                pass
        else:
            # Be crabby
            self.set_state("dislike_on")
            # TODO: wait rand_react_length() + until_respawn()

            self.set_state("like_off")
            _give_musicblock()
            _take_headphones()

        # TODO: trySetMetabolics(email, energy: -1, mood: 1,
        #  imgMin: (isRare ? 5 : 1), imgRange: 2)

        # Affect future listens
        self.add_to_history(item_type)
        self.busy_email = str()
