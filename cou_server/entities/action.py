from marshmallow import Schema, fields

from cou_server.entities.requirements import (
    ItemRequirements,
    ItemRequirementsSchema,
    SkillRequirements,
    SkillRequirementsSchema,
    EnergyRequirements,
    EnergyRequirementsSchema,
)


class Action:
    def __init__(self, action=None, action_word=None):
        if isinstance(action, str) or not action:
            if action:
                self.action_name = action
                self.custom_action_word = action_word

            self.enabled = True
            self.multi_enabled = False
            self.ground_action = False
            self.description = str()
            self.time_required = 0
            self.associated_skill = str()
            self.item_requirements = ItemRequirements()
            self.skill_requirements = SkillRequirements()
            self.energy_requirements = EnergyRequirements()
        elif isinstance(action, Action):
            self.action_name = action.action_name
            self.description = action.description
            self.custom_action_word = action.custom_action_word
            self.enabled = action.enabled
            self.multi_enabled = action.multi_enabled
            self.ground_action = action.ground_action
            self.time_required = action.time_required
            self.associated_skill = action.associated_skill
            self.item_requirements = ItemRequirements(req=action.item_requirements)
            self.skill_requirements = SkillRequirements(req=action.skill_requirements)
            self.energy_requirements = EnergyRequirements(
                req=action.energy_requirements
            )
        else:
            type_found = type(action).__name__
            raise TypeError(
                "Unsupported type passed to Action initializer: " + type_found
            )

    @property
    def action_word(self):
        return (
            self.custom_action_word
            if self.custom_action_word
            else self.action_name.lower()
        )

    def __str__(self):
        string = f"{self.action_name} requires any of {self.item_requirements.any}, "
        string += f"all of {self.item_requirements.all}, and at least "

        for skill, level in self.skill_requirements:
            string += f"{level} level of {skill}, "

        return string[0:-1]


class ActionSchema(Schema):
    actionName = fields.String(attribute="action_name")
    _actionWord = fields.String(attribute="action_word")
    error = fields.String()
    enabled = fields.Boolean()
    multiEnabled = fields.Boolean(attribute="multi_enabled")
    groundAction = fields.Boolean(attribute="ground_action")
    description = fields.String()
    timeRequired = fields.Integer(attribute="time_required")
    itemRequirements = fields.Nested(
        ItemRequirementsSchema, attribute="item_requirements"
    )
    skillRequirements = fields.Nested(
        SkillRequirementsSchema, attribute="skill_requirements"
    )
    energyRequirements = fields.Nested(
        EnergyRequirementsSchema, attribute="energy_requirements"
    )
    associatedSkill = fields.String(attribute="associated_skill")
