import random

from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.trees.tree import Tree


class BeanTree(Tree):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Bean Tree"
        self.reward_item_type = "bean"
        self.responses = {
            "harvest": [
                "Is that what you've bean looking for?",
                "Cool. Beans. Cool beans!",
                "Two bean, or not two bean?…",
                'You favored us. Now, we fava you. Ha ha. Like "fava bean".',
                "Wassssss-sap! Ha ha ha. Oh just take bean then.",
                "Have you seen Jack? I think I gave him the wrong seeds…",
                "I've bean watching you.",
            ],
            "pet": [
                "The petting is unbeleafable. Ha ha. Tree made joke. Laugh.",
                "Tiny Urling is very poplar with us. Ha ha.",
                "Your petting's never bean better. Hee!",
                "I wooden have thought you'd be so good. Now laugh.",
                "Tree arbors strong feelings to you. Chuckle now, please.",
                "Well it’s bean fun! See you later.",
                "We've all bean here before",
            ],
            "water": [
                "Water nice thing to do. Ha! Ha ha?",
                "Trunk you very much. Ha ha. We made joke. Laugh.",
                "Thought you'd never pull the twigger. Joke.",
                "Cheers, bud.",
                'How kind you\'re bean. Ha ha. "Bean".',
                "Where have you bean all my life?",
            ],
        }

        self.states = {
            "maturity_1": SpriteSheet(
                "maturity_1",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_1_seed_0_191991191_png_1354829640.png",
                990,
                540,
                198,
                270,
                9,
                False,
            ),
            "maturity_2": SpriteSheet(
                "maturity_2",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_2_seed_0_191991191_png_1354829642.png",
                990,
                540,
                198,
                270,
                9,
                False,
            ),
            "maturity_3": SpriteSheet(
                "maturity_3",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_3_seed_0_191991191_png_1354829643.png",
                990,
                540,
                198,
                270,
                9,
                False,
            ),
            "maturity_4": SpriteSheet(
                "maturity_4",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_4_seed_0_191991191_png_1354829645.png",
                990,
                2430,
                198,
                270,
                41,
                False,
            ),
            "maturity_5": SpriteSheet(
                "maturity_5",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_5_seed_0_191991191_png_1354829648.png",
                990,
                2970,
                198,
                270,
                51,
                False,
            ),
            "maturity_6": SpriteSheet(
                "maturity_6",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_6_seed_0_191991191_png_1354829652.png",
                990,
                3240,
                198,
                270,
                57,
                False,
            ),
            "maturity_7": SpriteSheet(
                "maturity_7",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_7_seed_0_191991191_png_1354829655.png",
                990,
                3240,
                198,
                270,
                59,
                False,
            ),
            "maturity_8": SpriteSheet(
                "maturity_8",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_8_seed_0_191991191_png_1354829659.png",
                990,
                3510,
                198,
                270,
                65,
                False,
            ),
            "maturity_9": SpriteSheet(
                "maturity_9",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_9_seed_0_191991191_png_1354829664.png",
                990,
                3780,
                198,
                270,
                66,
                False,
            ),
            "maturity_10": SpriteSheet(
                "maturity_10",
                "https://childrenofur.com/assets/entityImages/trant_bean__f_cap_10_f_num_10_h_10_m_10_seed_0_191991191_png_1354829669.png",
                990,
                3780,
                198,
                270,
                68,
                False,
            ),
        }

        self.maturity = random.randint(1, len(self.states))
        self.set_state(f"maturity_{self.maturity}")
        self.state = random.randint(0, self.current_state.num_frames)
        self.max_state = self.current_state.num_frames - 1

    def harvest(self, socket=None, email=None):
        success = super().harvest(socket=socket, email=email)

        # TODO
        # if success:
        #     harvested = StatManager.add(email, Stat.beans_harvested)
        #     if harvested >= 5003:
        #         Achievement.find('master_bean_counter').award_to(email)
        #     elif harvested >= 1009:
        #         Achievement.find('bean_counter_pro').award_to(email)
        #     elif harvested >= 503:
        #         Achievement.find('bean_counter').award_to(email)
        #     elif harvested >= 101:
        #         Achievement.find('participant_award_bean_division').award_to(email)

        return success

    def pet(self, socket=None, email=None):
        success = super().pet(socket=socket, email=email)

        # TODO
        # if success:
        #     stat = StatManager.add(email, Stat.bean_trees_petted)
        #     if stat >= 127:
        #         Achievement.find('professional_bean_tree_fondler').award_to(email)
        #     elif stat >= 41:
        #         Achievement.find('notquitepro_bean_tree_fondler').award_to(email)
        #     elif stat >= 11:
        #         Achievement.find('amateur_bean_tree_fondler').award_to(email)

        return success

    def water(self, socket=None, email=None):
        success = super().water(socket=socket, email=email)

        # TODO
        # if success:
        #     stat = StatManager.add(email, Stat.bean_trees_watered)
        #     if stat >= 41:
        #         Achievement.find('betterthanlousy_douser').award_to(email)

        return success
