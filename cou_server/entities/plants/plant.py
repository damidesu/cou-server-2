from datetime import datetime

from cou_server.entities.action import ActionSchema
from cou_server.entities.entity import Entity


class Plant(Entity):
    """Will check for plant growth/decay and send updates to clients if needed

	The actions map key string should be equivalent to the name of a function
	as it will be dynamically called in street_update_handler when the client
	attempts to perform one of the available actions;
	"""

    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        self.id = id
        self.x = x
        self.y = y
        self.z = z
        self.rotation = rotation
        self.h_flip = h_flip
        self.street_name = street_name
        self.respawn = datetime.now()
        self.actions = []
        self.states = None
        self.current_state = None

    def restore_state(self, metadata):
        if "state" in metadata:
            self.state = int(metadata["state"])

        if "currentState" in metadata:
            self.set_state(metadata["currentState"])

    def get_persist_metadata(self):
        return {"state": str(self.state), "currentState": self.current_state.state_name}

    def update(self, simulate_tick=False):
        pass

    def get_map(self):
        map = super().get_map()
        map["url"] = self.current_state.url
        map["id"] = self.id
        map["type"] = self.type
        map["state"] = self.state
        map["numRows"] = self.current_state.num_rows
        map["numColumns"] = self.current_state.num_cols
        map["numFrames"] = self.current_state.num_frames
        map["actions"] = ActionSchema().dump(self.actions, many=True)
        map["x"] = self.x
        map["y"] = self.y
        map["z"] = self.z
        map["rotation"] = self.rotation
        map["h_flip"] = self.h_flip
        return map
