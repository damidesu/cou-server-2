from marshmallow import Schema, fields


class SkillRequirements:
    def __init__(self, req=None):
        if isinstance(req, SkillRequirements):
            # Clone
            self.required_skill_levels = dict(req.required_skill_levels)
            self.error = req.error
        elif not req:
            # Initialize
            self.required_skill_levels = dict()
            self.error = "You don't have the required skill(s)"
        else:
            raise TypeError("Unsupported type passed to SkillRequirements initializer")

    def __iter__(self):
        for skill, level in self.required_skill_levels:
            yield skill, level


class SkillRequirementsSchema(Schema):
    requiredSkillLevels = fields.Dict(
        keys=fields.String, values=fields.Integer, attribute="required_skill_levels"
    )
    error = fields.String()


class ItemRequirements:
    def __init__(self, req=None):
        if isinstance(req, ItemRequirements):
            # Clone
            self.any = list(req.any)
            self.all = dict(req.all)
            self.error = req.error
        elif not req:
            # Initialize
            self.any = list()
            self.all = dict()
            self.error = "You don't have the required item(s)"
        else:
            type_found = type(req).__name__
            raise TypeError(
                "Unsupported type passed to ItemRequirements initializer: " + type_found
            )

    def set(self, req_any: list, req_all: dict):
        self.any = req_any
        self.all = req_all


class ItemRequirementsSchema(Schema):
    any = fields.List(fields.String)
    all = fields.Dict(keys=fields.String, values=fields.Integer)
    error = fields.String()


class EnergyRequirements:
    def __init__(self, req=None):
        if isinstance(req, EnergyRequirements):
            # clone
            self.energy_amount = req.energy_amount
            self.error = req.error
        elif isinstance(req, int) or not req:
            self.energy_amount = req if req else 0
            self.error = (
                f"You need at least {self.energy_amount} energy to perform this action"
            )
        else:
            type_found = type(req).__name__
            raise TypeError(
                "Unsupported type passed to EnergyRequirements initializer: "
                + type_found
            )


class EnergyRequirementsSchema(Schema):
    energyAmount = fields.Integer(attribute="energy_amount")
    error = fields.String()
