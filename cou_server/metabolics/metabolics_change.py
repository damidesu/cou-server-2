import random

from cou_server.common.database import db_session_context
from cou_server.common.util import clamp
from cou_server.models.user import User


class MetabolicsChange:
    def __init__(self):
        self.gains = {"energy": 0, "mood": 0, "img": 0, "currants": 0}

    def try_set_metabolics(
        self, email, rewards=None, energy=0, mood=0, img_min=0, img_range=0, currants=0
    ):
        """ Try to set the metabolics belongning to [email]. If [rewards] is set,
            it will take precedence over the other metabolics passed in.
        """

        self.reset_gains()

        if rewards:
            energy = rewards.energy
            mood = rewards.mood
            img_min = rewards.img
            currants = rewards.currants
            self.try_set_favor(email, None, None, favors=rewards.favor)

        with db_session_context() as db_session:
            m = db_session.query(User).filter(User.email == email).first().metabolics

            # if we're taking away energy, make sure we have enough
            if energy < 0 and m.energy < abs(energy):
                return False

            m.energy += energy
            m.energy = clamp(m.energy, m.max_energy, 0)
            m.mood += mood
            m.mood = clamp(m.mood, m.max_mood, 0)
            m.currants += currants
            base_img = img_min
            if img_range > 0:
                base_img = random.randint(0, img_range) + img_min
            result_img = (base_img * m.mood / m.max_mood) // 1
            m.img += result_img
            m.lifetime_img += result_img

        # send results to client
        self.gains["energy"] = energy
        self.gains["mood"] = mood
        self.gains["img"] = result_img
        self.gains["currants"] = currants

        return True

    def try_set_favor(self, email, giant_name, fav_amt, favors=None):
        metabolics = get_metabolics(email=email)

        if favors:
            for favor in favors:
                metabolics = self._set_favor(
                    email, metabolics, favor.giant_name, favor.fav_amt
                )
        else:
            metabolics = self._set_favor(email, metabolics, giant_name, fav_amt)

        set_metabolics(metabolics)
        return metabolics

    def reset_gains(self):
        self.gains = {"energy": 0, "mood": 0, "img": 0, "currants": 0}

    def _set_favor(self, email, metabolics, giant_name, fav_amt):
        giant_favor = getattr(metabolics, f"{giant_name}_favor")
        max_amt = getattr(metabolics, f"{giant_name}_favor_max")

        if giant_favor + fav_amt >= max_amt:
            setattr(metabolics, f"{giant_name}_favor", 0)
            max_amt += 100
            setattr(metabolics, f"{giant_name}_favor_max", max_amt)
            # TODO
            # InventoryV2.addItemToUser(email, items['emblem_of_' + giantName.toLowerCase()].getMap(), 1);
            # Achievement.find("first_emblem_of_${giantName.toLowerCase()}").awardTo(email);

            # //end emblem quest
            # messageBus.publish(new RequirementProgress('emblemGet', email));
            # StatManager.add(email, Stat.emblems_collected);
        else:
            setattr(metabolics, f"{giant_name}_favor", giant_favor + fav_amt)

        # StatManager.add(email, Stat.favor_earned, increment: favAmt);
        return metabolics

    def get_mood(self, username=None, email=None, user_id=None):
        if not username and not email and not user_id:
            raise ValueError("You must pass either username, email, or user_id")

        return get_metabolics(username=username, email=email, user_id=user_id).mood

    def get_energy(self, username=None, email=None, user_id=None):
        if not username and not email and not user_id:
            raise ValueError("You must pass either username, email, or user_id")

        return get_metabolics(username=username, email=email, user_id=user_id).energy
