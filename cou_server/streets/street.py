import json

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import reconstructor

from cou_server.common import get_logger
from cou_server.common.database import Base, db_session_context
from cou_server.endpoints import mapdata, street_update
from cou_server import entities
from cou_server.entities.doors.door import Door
from cou_server.entities.items.item import Item
from cou_server.entities.npcs.npc import NPC
from cou_server.entities.plants.plant import Plant
from cou_server.entities.quoin import Quoin
from cou_server.models.entity import Entity


logger = get_logger(__name__)


class Rectangle:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def __str__(self):
        return f"At {self.x}, {self.y}: {self.width}x{self.height}"

    def __eq__(self, other):
        return (
            self.x == other.x
            and self.y == other.y
            and self.wdith == other.width
            and self.height == other.height
        )


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"A point {self.x},{self.y}"


class Wall:
    def __init__(self, wall: dict, layer: dict, ground_y: int):
        self.width = wall["w"]
        self.height = wall["h"]
        self.x = wall["x"] + layer["w"] // 2 - width // 2
        self.y = wall["y"] + layer["y"] + ground_y
        self.id = wall["id"]

        self.bounds = Rectangle(x, y, width, height)

    def __str__(self):
        return f"wall {self.id}: {self.bounds}"


class CollisionPlatform:
    def __init__(self, platform_line: dict, layer: dict, ground_y: int):
        self.id = platform_line["id"]
        self.ceiling = platform_line["platform_pc_perm"] == 1
        self.item_perm = True

        for endpoint in platform_line["endpoints"]:
            if endpoint["name"] == "start":
                self.start = Point(endpoint["x"], endpoint["y"] + ground_y)
                if layer["name"] == "middleground":
                    self.start = Point(
                        endpoint["x"] + layer["w"] // 2,
                        endpoint["y"] + layer["h"] + ground_y,
                    )
            if endpoint["name"] == "end":
                self.end = Point(endpoint["x"], endpoint["y"] + ground_y)
                if layer["name"] == "middleground":
                    self.end = Point(
                        endpoint["x"] + layer["w"] // 2,
                        endpoint["y"] + layer["h"] + ground_y,
                    )

        width = self.end.x - self.start.x
        height = self.end.y - self.start.y
        self.bounds = Rectangle(self.start.x, self.start.y, width, height)

    def __str__(self):
        return f"({self.start.x},{self.start.y})->({self.end.x},{self.end.y}) ceiling={self.ceiling}"

    def __eq__(self, other):
        return other.bounds == self.bounds

    def __hash__(self):
        return hash(self.start.x + self.start.y + self.end.x + self.end.y)

    def __lt__(self, other):
        return other.start.y - self.start.y


class Street(Base):
    __tablename__ = "streets"

    _json_cache = {}
    _persist_lock = {}

    id = Column(String, primary_key=True)
    items = Column(String, default="[]")
    uid = Column(Integer, nullable=False, default=-1)
    tsid = Column(String)

    @reconstructor
    def on_load(self):
        self.loaded = False
        self.occupants = {}
        self.platforms = []
        self.walls = []
        self.ground_y = 0
        self.quoins = {}
        self.plants = {}
        self.npcs = {}
        self.doors = {}
        self.entity_maps = {
            "quoin": self.quoins,
            "plant": self.plants,
            "npc": self.npcs,
            "door": self.doors,
            "groundItem": self.ground_items,
        }

        self.label = mapdata.get_street_by_tsid(self.tsid)["label"]
        self.load_entities()

    def __str__(self):
        return f"This is a Street with an id of {self.id} which has {len(self.ground_items)} items on it"

    @property
    def ground_items(self):
        items = []
        for item_map in json.loads(self.items):
            items.append(Item(**item_map))
        return items

    @ground_items.setter
    def ground_items(self, new_items):
        self.items = json.dumps(new_items)

    def load_items(self):
        for item in self.ground_items:
            item.put_item_on_ground(item.x, item.y, self.street_name, id=item.item_id)

    def load_json(self, refresh_cache=False):
        tsid_g = mapdata.tsid_g(self.tsid)
        street_data = Street._json_cache.get(tsid_g, {})
        if refresh_cache or tsid_g not in Street._json_cache:
            street_data = mapdata.get_and_cache_file(self.tsid)
            Street._json_cache[tsid_g] = street_data

        ground_y = -abs(int((street_data["dynamic"]["ground_y"])))
        bounds = Rectangle(
            street_data["dynamic"]["l"],
            street_data["dynamic"]["t"],
            abs(street_data["dynamic"]["l"]) + abs(street_data["dynamic"]["r"]),
            abs(street_data["dynamic"]["t"] - street_data["dynamic"]["b"]),
        )

        # for each layer on the street...
        for layer in street_data["dynamic"]["layers"].values():
            for platform_line in layer["platformLines"]:
                self.platforms.append(CollisionPlatform(platform_line, layer, ground_y))

            self.platforms.sort()

            for wall in layer["walls"]:
                if wall["pc_perm"] == 0:
                    continue
                self.walls.append(Wall(wall, layer, ground_y))

    def load_entities(self):
        with db_session_context() as db_session:
            entities = (
                db_session.query(Entity)
                .filter(Entity.tsid == mapdata.tsid_l(self.tsid))
                .all()
            )
            if not entities:
                return
            self.put_entities_in_memory(entities)

    def put_entities_in_memory(self, entities_to_load):
        for entity in entities_to_load:
            type = entity.type
            x = entity.x
            y = entity.y
            z = entity.z
            rotation = entity.rotation
            h_flip = entity.h_flip
            id = entity.id
            metadata = json.loads(entity.metadata_json)

            if type in [
                "Img",
                "Mood",
                "Energy",
                "Currant",
                "Mystery",
                "Favor",
                "Time",
                "Quarazy",
            ]:
                self.quoins[id] = Quoin(id, x, y, type.lower())
            else:
                try:
                    klass = getattr(entities, type)
                except AttributeError:
                    # this class isn't defined yet (code still under development)
                    logger.debug(f"Could not load a class for {type}")
                    continue
                try:
                    kwargs = {
                        "street_name": self.label,
                        "id": id,
                        "x": x,
                        "y": y,
                        "z": z,
                        "rotation": rotation,
                        "h_flip": h_flip,
                    }
                    entity_instance = klass(**kwargs)
                    entity_instance.restore_state(metadata)
                    if issubclass(klass, NPC):
                        self.npcs[id] = entity_instance
                    elif issubclass(klass, Plant):
                        self.plants[id] = entity_instance
                    elif issubclass(klass, Door):
                        self.doors[id] = entity_instance
                except Exception:
                    logger.exception(f"Unable to instantiate a class for {type}")
                    return False

        return True

    def simulate(self):
        if not self.occupants:
            return False

        updates = {
            "label": self.label,
            "quoins": [],
            "npcs": [],
            "plants": [],
            "doors": [],
            "groundItems": [],
        }

        for plant in self.plants.values():
            plant.update(simulate_tick=True)
            updates["plants"].append(plant.get_map())
        for quoin in self.quoins.values():
            quoin.update(simulate_tick=True)
            updates["quoins"].append(quoin.get_map())
        for door in self.doors.values():
            updates["doors"].append(door.get_map())
        for npc in self.npcs.values():
            npc.update(simulate_tick=True)

        for username, web_socket in self.occupants.items():
            if not web_socket:
                continue

            custom_updates = dict(updates)
            custom_updates["npcs"] = []
            for npc in self.npcs:
                custom_updates["npcs"].append(npc.get_map(username))
            try:
                web_socket.send(json.dumps(custom_updates))
            except Exception:
                pass

        return True
