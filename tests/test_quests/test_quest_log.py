import pytest

from cou_server.common.message_bus import MessageBus
from cou_server.endpoints import quest as quest_service
from cou_server.models.user import User
from cou_server.quests import messages
from cou_server.quests import quest as quest_module
from cou_server.quests.quest import Quest
from cou_server.quests.quest_log import QuestLog, QuestLogSchema
from cou_server.quests import requirement as requirement_module
from cou_server.quests.requirement import Requirement


def test_quest_log_serializable():
    """ Verify we can serialize the quest log object. """

    quest_log = QuestLog(
        id=1,
        user=User(id=1),
        in_progress_quests=[Quest(id="Q1")],
        completed_quests=[Quest(id="Q2")],
    )
    print("quest log only id=" + str(QuestLogSchema(only=("id",)).dump(quest_log)))
    assert QuestLogSchema(only=("id",)).dump(quest_log) == {"id": 1}
    log = QuestLogSchema(only=("id", "user")).dump(quest_log)
    assert log["id"] == 1
    assert log["user"]["id"] == 1
    log = QuestLogSchema(only=("id", "in_progress_quests")).dump(quest_log)
    assert log["id"] == 1
    assert log["in_progress_quests"][0]["id"] == "Q1"
    log = QuestLogSchema(only=("id", "completed_quests")).dump(quest_log)
    assert log["id"] == 1
    assert log["completed_quests"][0]["id"] == "Q2"


def test_quest_log_complete_quest():
    """ Verify that a quest being tracked by the quest log can tell it that it
        was completed.
    """

    requirement = Requirement(id="R1")
    quest = Quest(id="Q1", requirements=[requirement])
    quest_log = QuestLog(in_progress_quests=[quest])
    quest_log.start_tracking("foo@bar.com")
    assert quest.being_tracked
    assert requirement.being_tracked
    assert quest_log.completed_quests == []

    # quest should be completed and so put in the completed_quests lists
    requirement.fulfilled = True
    assert quest in quest_log.completed_quests


def test_quest_log_fail_quest(monkeypatch):
    """ Verify that a timed quest can fail """

    class MockTimer:
        def __init__(self, time_limit, callback):
            callback()

    monkeypatch.setattr(requirement_module, "Timer", MockTimer)

    requirement = Requirement(id="R1", req_type="timed", time_limit=1)
    quest = Quest(id="Q1", requirements=[requirement])
    quest_log = QuestLog(in_progress_quests=[quest])
    quest_log.start_tracking("foo@bar.com")
    assert quest_log.in_progress_quests == []
    assert not quest.being_tracked


def test_quest_log_handle_requirement_updated(monkeypatch):
    """ Verify that a requirement update event will be sent to the user """

    quest_log_sent = False
    email = "foo@bar.com"
    event_type = "test_event"

    def mock_update_quest_log(quest_log):
        nonlocal quest_log_sent
        quest_log_sent = True

    monkeypatch.setattr(quest_service, "update_quest_log", mock_update_quest_log)

    requirement = Requirement(
        id="R1", num_required=2, event_type=event_type, req_type="counter"
    )
    quest = Quest(id="Q1", requirements=[requirement])
    quest_log = QuestLog(in_progress_quests=[quest])
    quest_log.start_tracking(email)

    MessageBus().publish(
        "requirement_progress", messages.RequirementProgress(event_type, email)
    )
    assert quest_log_sent


@pytest.mark.parametrize("channel", ["accept_quest", "reject_quest"])
def test_quest_log_accept_quest(monkeypatch, channel):
    """ Verify the user can accept or reject a quest offer """

    email = "foo@bar.com"
    quest_id = "Q1"

    monkeypatch.setattr(quest_module, "quests", {quest_id: Quest(id=quest_id)})

    quest_log = QuestLog()
    quest_log.start_tracking(email)
    quest_log.offer_quest(quest_id)
    assert quest_log.offering_quest
    MessageBus().publish(channel, messages.AcceptQuest(quest_id, email))
    assert not quest_log.offering_quest
    if channel == "accept_quest":
        assert Quest(id=quest_id) in quest_log.in_progress_quests
    assert not [s for s in quest_log.mb_subscriptions if s.channel == channel]


def test_quest_log_stop_tracking():
    """ Verify that all of the quests are not tracked anymore when the quest log
        is not tracked anymore.
    """

    requirement = Requirement(id="R1")
    quest = Quest(id="Q1", requirements=[requirement])
    quest_log = QuestLog(in_progress_quests=[quest])
    quest_log.start_tracking("foo@bar.com")
    assert quest.being_tracked
    assert requirement.being_tracked
    assert quest_log.being_tracked
    quest_log.stop_tracking()
    assert not quest.being_tracked
    assert not requirement.being_tracked
    assert not quest_log.being_tracked
