import pytest

from cou_server.common.message_bus import MessageBus, subscribe
from cou_server.quests import messages
from cou_server.quests.requirement import Requirement, RequirementSchema


def test_requirement_equality():
    """ Verify two requirements are equal to each other when their IDs have the
        same value.
    """

    requirement1 = Requirement(id="R1")
    requirement2 = Requirement(id="R1")
    assert requirement1 == requirement2
    assert hash(requirement1) == hash(requirement2)
    assert RequirementSchema().dump(requirement1) == RequirementSchema().dump(
        requirement2
    )
    assert Requirement.clone(requirement1) == requirement2
    print(requirement1, requirement2)


def test_requirement_complete_publish():
    """ Verify that fulfilling a requirement publishes a message when it's
        being tracked.
    """

    complete_received = False
    email = "foo@bar.com"

    @subscribe("complete_requirement")
    def test_get_message(data):
        assert data.email == email
        nonlocal complete_received
        complete_received = True

    requirement = Requirement(id="R1")
    requirement.fulfilled = True
    assert not complete_received
    requirement.fulfilled = False
    requirement.start_tracking(email)
    requirement.fulfilled = True
    assert complete_received


def test_requirement_counter_publish():
    """ Verify that when the num_fullfilled is set to the num_required, then the
        requirement will be completed.
    """

    complete_received = False
    email = "foo@bar.com"

    @subscribe("complete_requirement")
    def test_get_message(data):
        assert data.email == email
        nonlocal complete_received
        complete_received = True

    requirement = Requirement(id="R1")
    requirement.num_required = 1
    requirement.start_tracking(email)
    assert requirement.num_fullfilled == 0
    requirement.num_fullfilled += 1
    assert complete_received


@pytest.mark.parametrize("req_type", ["counter", "counter_unique", "timed"])
def test_requirement_handle_requirement_progress(req_type):
    """ Verify that if a requiement hears the requirement_progress message, it
        will decide if it needs to publish its own message in response.
    """

    update_received = 0
    complete_received = 0
    email = "foo@bar.com"
    event_type = "test_event"

    @subscribe("complete_requirement")
    @subscribe("requirement_updated")
    def test_get_message(data):
        assert data.email == email
        nonlocal update_received, complete_received
        if isinstance(data, messages.RequirementUpdated):
            update_received += 1
        if isinstance(data, messages.CompleteRequirement):
            complete_received += 1

    requirement = Requirement(
        id="R1", num_required=2, event_type=event_type, req_type=req_type
    )
    requirement.start_tracking(email)
    MessageBus().publish(
        "requirement_progress",
        messages.RequirementProgress(event_type, "different_email"),
    )
    MessageBus().publish(
        "requirement_progress", messages.RequirementProgress(event_type, email)
    )
    assert update_received == 1
    assert complete_received == 0
    MessageBus().publish(
        "requirement_progress", messages.RequirementProgress(event_type, email)
    )
    if req_type == "counter" or req_type == "timed":
        assert update_received == 2
        assert complete_received == 1
        assert requirement.fulfilled
    else:
        assert update_received == 1
        assert complete_received == 0
        assert not requirement.fulfilled
        MessageBus().publish(
            "requirement_progress",
            messages.RequirementProgress("different_test_event", email),
        )
        assert update_received == 2
        assert complete_received == 1
        assert requirement.fulfilled
