import pytest

from cou_server.quests import quest as quest_module
from cou_server.quests.quest import Quest, QuestSchema
from cou_server.quests.requirement import Requirement


def test_quest_equality(monkeypatch):
    """ Verify two quests are equal to each other when their IDs have the
        same value.
    """

    monkeypatch.setattr(quest_module, "quests", {"Q1": Quest(id="Q1")})

    quest1 = Quest(id="Q1")
    quest2 = Quest(id="Q1")
    assert quest1 == quest2
    assert hash(quest1) == hash(quest2)
    assert QuestSchema().dump(quest1) == QuestSchema().dump(quest2)
    assert Quest.clone("Q1") == quest2
    print(quest1, quest2)


def test_quest_completed(monkeypatch):
    """ Verify the quest completes if all of its requirements are fulfilled """

    rewards_sent = False

    def mock_give_rewards(self):
        nonlocal rewards_sent
        rewards_sent = True

    monkeypatch.setattr(Quest, "_give_rewards", mock_give_rewards)

    quest = Quest(id="Q1", requirements=[Requirement(id="R1")])
    quest.start_tracking("foo@bar.com")
    assert not rewards_sent
    quest.requirements[0].fulfilled = True
    assert rewards_sent
