from cou_server import server
import pytest

from cou_server.common.database import Base, LocalSession, local_engine
from cou_server.common.message_bus import MessageBus


@pytest.fixture(scope="function")
def db_session(request):
    Base.metadata.create_all(local_engine)
    session = LocalSession()
    yield session
    Base.metadata.drop_all(local_engine)


@pytest.fixture(scope="function", autouse=True)
def clear_bus(request):
    MessageBus().subscribers = []


@pytest.fixture(scope="function")
def test_client(request):
    client = server.app.test_client()
    yield client
