from gevent.pywsgi import WSGIServer

from cou_server.server import main


def test_main(monkeypatch):
    """ Verify that the application will start up """

    def mock_serve_forever(*args, **kwargs):
        return 0

    monkeypatch.setattr(WSGIServer, "serve_forever", mock_serve_forever)

    assert main() is None
