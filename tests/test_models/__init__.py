# Add model import statements to this file to make sure that all of the DB classes
# are part of the test environment

from .test_resource import UTResource

__all__ = ["UTResource"]
