import pytest

from cou_server.endpoints import mapdata


# Utility functions


def test_tsid_l():
    # Replace G with L
    assert mapdata.tsid_l("GIFF6BQE33H26JC") == "LIFF6BQE33H26JC"

    # Leave L alone
    assert mapdata.tsid_l("LIF18V95I972R96") == "LIF18V95I972R96"

    # Empty TSID
    with pytest.raises(ValueError):
        mapdata.tsid_l(str())


def test_tsid_g():
    # Replace L with G
    assert mapdata.tsid_g("LIF18V95I972R96") == "GIF18V95I972R96"

    # Leave G alone
    assert mapdata.tsid_g("GIFF6BQE33H26JC") == "GIFF6BQE33H26JC"

    # Empty TSID
    with pytest.raises(ValueError):
        mapdata.tsid_g(str())


def test_get_street_by_tsid():
    mapdata.read_map_data()

    # Starts with L
    street_1 = mapdata.get_street_by_tsid("LDO8NGHIFTQ21CQ")
    assert street_1["label"] == "Sunsitara Stona"
    assert int(street_1["hub_id"]) == 119

    # Starts with G
    street_2 = mapdata.get_street_by_tsid("GIF12PMQ5121D68")
    assert street_2["label"] == "Cebarkul"
    assert int(street_2["hub_id"]) == 51

    # Fake street
    with pytest.raises(ValueError):
        mapdata.get_street_by_tsid("AUFU58GHF26HDH8")

    # Empty TSID
    with pytest.raises(ValueError):
        mapdata.get_street_by_tsid(str())


def test_street_is_hidden():
    mapdata.read_map_data()

    # Not hidden
    assert not mapdata.street_is_hidden("Cebarkul")

    # Hub hidden
    assert mapdata.street_is_hidden("Gloomy_basement")

    # Street hidden
    assert mapdata.street_is_hidden("Pitika Parse Subway Station")

    # Empty label
    with pytest.raises(ValueError):
        mapdata.street_is_hidden(str())

    # Fake label
    with pytest.raises(ValueError):
        mapdata.street_is_hidden("Wall St")


# Specialty functions


def test_is_savanna_street():
    mapdata.read_map_data()

    # Yes
    assert mapdata.is_savanna_street("Contour Rd W")

    # No
    assert not mapdata.is_savanna_street("Mira Mesh")

    # Missing label
    with pytest.raises(ValueError):
        mapdata.is_savanna_street(str())

    # Fake street
    with pytest.raises(ValueError):
        mapdata.is_savanna_street("Wall St")


def test_savanna_escape_to():
    mapdata.read_map_data()

    # Escape from Baqala to Tamila
    assert mapdata.savanna_escape_to("Parsin's Trail E") == "LIF18V95I972R96"

    # Escape from Choru to Vantalu
    assert mapdata.savanna_escape_to("Putter Track A") == "LIFF6BQE33H26JC"

    # Escape from Xalanga to Folivoria
    assert mapdata.savanna_escape_to("Ramble Way 1") == "LDO8NGHIFTQ21CQ"

    # Escape from Zhambu to Tahli
    assert mapdata.savanna_escape_to("Rube's Way 1") == "LHF4QVGL7NI269C"

    # Non-savanna street escapes to itself
    assert mapdata.savanna_escape_to("Mira Mesh") == "LA58KK7B9O522PC"

    # Missing label
    with pytest.raises(ValueError):
        mapdata.is_savanna_street(str())

    # Fake street
    with pytest.raises(ValueError):
        mapdata.is_savanna_street("Wall St")


# Endpoints


def test_get_map_data(test_client):
    mapdata.read_map_data()

    response = test_client.get("/getMapData")
    assert response.status_code == 200
    assert "streets" in response.json
    assert "hubs" in response.json
    assert "render" in response.json


def test_list_streets(test_client):
    mapdata.read_map_data()

    response = test_client.get("/listStreets")
    assert response.status_code == 200

    response = test_client.get("/listStreets?all")
    assert response.status_code == 200


def test_get_street_data(test_client):
    mapdata.read_map_data()

    # Missing TSID
    response = test_client.get("/getStreet")
    assert response.status_code == 400

    # Fake TSID
    response = test_client.get("/getStreet?tsid=AUFU58GHF26HDH8")
    assert response.status_code == 410

    # Real TSID, online request
    response = test_client.get("/getStreet?tsid=LA58KK7B9O522PC")
    assert response.status_code == 200

    # Real TSID, offline request
    response = test_client.get("/getStreet?tsid=LA58KK7B9O522PC")
    assert response.status_code == 200
