import json

import pytest

from cou_server.common import util


def test_toast(monkeypatch):
    """ Verify that we can send a message which will be shown as a toast on the
        recipient's client.
    """

    toast_message = "ut toast message"
    toast_sent = False

    class MockWebsocket:
        def send(self, message):
            toast_data = json.loads(message)
            assert toast_data["toast"]
            assert toast_data["message"] == toast_message
            nonlocal toast_sent
            toast_sent = True

    util.toast(toast_message, MockWebsocket())
    assert toast_sent
